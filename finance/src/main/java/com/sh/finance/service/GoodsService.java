package com.sh.finance.service;

import com.sh.finance.entity.Goods;
import com.sh.finance.mapper.IGoodsMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import java.util.List;

/**
 * @Author: he changjie on 2019/3/8
 * @Description:
 * 库存货物
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class GoodsService {
    @Autowired
    private IGoodsMapper mapper;

    /**
     * 新增货物入库，若已存在的货物则数量增加
     * @param goods 货物实体
     * @return 1：成功
     */
    public int add(Goods goods){
        List<Goods> list = mapper.selectAll();
        String id = null;
        if(list!=null&&list.size()>0){
            for (Goods temp : list) {
                if(temp.getGoodsName().equals(goods.getGoodsName()) &&
                        temp.getMaterial().equals(goods.getMaterial()) &&
                        temp.getSpecification().equals(goods.getSpecification()) &&
                        temp.getSupplier().equals(goods.getSupplier()) &&
                        temp.getGoodsType().equals(goods.getGoodsType())  &&
                        temp.getPrice().equals(goods.getPrice())){
                    id=temp.getId();
                    break;
                }
            }
        }
        if(StringUtils.isEmpty(id)){
            goods.setFlag("0");
            return mapper.insertSelective(goods);
        }else{
            Goods one = queryOne(id);
            goods.setId(one.getId());
            if("0".equals(one.getFlag())){
                int a1=Integer.valueOf(one.getNumber());
                int a2=Integer.valueOf(goods.getNumber());
                one.setNumber(String.valueOf(a1+a2));
            }else{
                one.setFlag("0");
                one.setNumber(goods.getNumber());
            }
                return update(one);
        }
    }

    /**
     * 删除货物
     * @param id ID
     * @return 1:成功
     */
    public int delete(String id){
        Goods goods = queryOne(id);
        goods.setNumber("0");
        goods.setFlag("-1");
        return update(goods);
    }

    /**
     * 更新货物信息
     * @param goods  货物实体
     * @return 1:成功
     */
    public int update(Goods goods){
        return mapper.updateByPrimaryKeySelective(goods);
    }

    /**
     * 货物出库
     * @param id ID
     * @param num 数量
     */
    public int less(String id, String num){
        Goods one = queryOne(id);
        int number = Integer.valueOf(one.getNumber());
        if(Integer.valueOf(num)>=number){
            return delete(id);
        }else{
            number-=Integer.valueOf(num);
            one.setNumber(String.valueOf(number));
            return update(one);
        }
    }

    /**
     * 通过ID查询货物
     * @param id 货物ID
     * @return 货物
     */
    public Goods queryOne(String id){
        return mapper.selectByPrimaryKey(id);
    }

    /**
     * 分页
     * 搜索符合条件的入库信息
     * @param goodsName 货物名称
     * @param currentPage 当前页号
     * @param pageSize 页尺寸
     * @return
     */
    public List<Goods> queryAll(String goodsName, int currentPage, int pageSize){
        RowBounds rowBunds = new RowBounds((currentPage - 1) * pageSize, pageSize);
        Example example=new Example(Goods.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("flag","0");
        if(StringUtils.isNotEmpty(goodsName)){
            criteria.andLike("goodsName","%"+goodsName+"%");
        }
        return mapper.selectByExampleAndRowBounds(example,rowBunds);
    }

    /**
     * 分页
     * 搜索符合条件的入库信息的数量
     * @param goodsName 货物名称
     * @return 数量
     */
    public int queryAll(String goodsName){
        Example example=new Example(Goods.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("flag","0");
        if(StringUtils.isNotEmpty(goodsName)){
            criteria.andLike("goodsName","%"+goodsName+"%");
        }
        return mapper.selectCountByExample(example);
    }
}
