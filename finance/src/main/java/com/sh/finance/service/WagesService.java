package com.sh.finance.service;

import com.google.common.collect.Maps;
import com.sh.finance.common.JsonMapper;
import com.sh.finance.common.Result;
import com.sh.finance.entity.Payment;
import com.sh.finance.entity.Wages;
import com.sh.finance.entity.WorkPlan;
import com.sh.finance.interfaces.PaymentInterface;
import com.sh.finance.interfaces.WorkInterface;
import com.sh.finance.mapper.IWagesMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: he changjie on 2019/3/8
 * @Description:
 * 工作核算
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class WagesService {
    private JsonMapper jsonMapper = JsonMapper.INSTANCE;
    @Autowired
    private IWagesMapper mapper;
    @Autowired
    private PaymentInterface paymentInterface;
    @Autowired
    private WorkInterface workInterface;

    /**
     * 新增一条工作核算记录
     * @param wages 工作核算实体
     * @return 1;成功
     */
    public int add(Wages wages){
        return mapper.insertSelective(wages);
    }

    /**
     * 更新一条工作核算记录
     * @param wages 工作核算实体
     * @return  1;成功
     */
    public int update(Wages wages){
        return mapper.updateByPrimaryKeySelective(wages);
    }

    /**
     *  通过ID查询一条工作核算记录
     * @param id ID
     * @return 工作核算实体
     */
    public Wages queryOne(String id){
        return mapper.selectByPrimaryKey(id);
    }

    /**
     * 通过员工ID和工作期数获取固定工资和计件说明
     * @param id  员工ID
     * @param time 工作期数
     * @return Map<String,String>
     */
    public Map<String,String> queryMoney(String id, String time){
        HashMap<String, String> resultMap = Maps.newHashMap();

        Result<Payment> payments = paymentInterface.queryOne(id);
        if(payments!=null && payments.getData()!=null){
            resultMap.put("fixedSalary",payments.getData().getPay());
        }else{
            resultMap.put("fixedSalary","");
        }

        HashMap<Object, Object> params = Maps.newHashMap();
        //计算时间区间
        int year=Integer.valueOf(time.split("-")[0]);
        int month=Integer.valueOf(time.split("-")[1]);
        Calendar cal = Calendar.getInstance();
        //设置年份
        cal.set(Calendar.YEAR,year);
        //设置月份
        cal.set(Calendar.MONTH, month-1);
        //获取某月最小天数
        int minimum = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
        //设置日历中月份的最小天数
        cal.set(Calendar.DAY_OF_MONTH, minimum);
        //格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String beginTime = sdf.format(cal.getTime());
        int maximum = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        cal.set(Calendar.DAY_OF_MONTH, maximum);
        String endTime=sdf.format(cal.getTime());
        //end 计算时间区间
        params.put("id",id);
        params.put("beginTime",beginTime);
        params.put("endTime",endTime);
        Result<List<WorkPlan>> works = workInterface.queryAllS(jsonMapper.toJson(params));
        if(works!=null && works.getData()!=null && works.getData().size()>0){
            StringBuilder sf=new StringBuilder();
            for (WorkPlan plan : works.getData()) {
                int num = plan.getRecipientId().split(",").length;
                String workName = plan.getWorkName();
                if(num==1){
                    sf.append(workName).append("一人完成\n");
                }else{
                    sf.append(workName).append("由").append(num).append("人合作完成，占1/").append(num).append("\n");
                }
            }
            resultMap.put("pieceDesc",sf.toString());
        }else{
            resultMap.put("pieceDesc","");
        }
        return resultMap;
    }

    /**
     * 分页
     * 搜索符合条件的工作核算记录
     * @param search 员工姓名
     * @param currentPage 当前页号
     * @param pageSize 页尺寸
     * @return List<Wages>
     */
    public List<Wages> queryAll(String search,int currentPage,int pageSize){
        RowBounds rowBunds = new RowBounds((currentPage - 1) * pageSize, pageSize);
        Example example=new Example(Wages.class);
        example.setOrderByClause("periods desc");
        Example.Criteria criteria = example.createCriteria();
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("staffName","%"+search+"%");
        }
        return mapper.selectByExampleAndRowBounds(example,rowBunds);
    }

    /**
     * 分页
     * 搜索符合条件的工作核算记录的数量
     * @param search 员工姓名
     * @return  数量
     */
    public int queryAll(String search){
        Example example=new Example(Wages.class);
        example.setOrderByClause("periods desc");
        Example.Criteria criteria = example.createCriteria();
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("staffName","%"+search+"%");
        }
        return mapper.selectCountByExample(example);
    }


}
