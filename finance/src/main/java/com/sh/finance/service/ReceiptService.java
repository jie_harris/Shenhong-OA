package com.sh.finance.service;

import com.sh.finance.entity.Receipt;
import com.sh.finance.mapper.IReceiptMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Author: he changjie on 2019/3/8
 * @Description:
 * 资金管理
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ReceiptService {
    @Autowired
    private IReceiptMapper mapper;
    @Autowired ExpenseService expenseService;

    /**
     * 新增一个款单
     * @param receipt 款单实体
     * @return 1：成功
     */
    public int add(Receipt receipt){
        receipt.setConfirmationStatus("未确认");
        return mapper.insertSelective(receipt);
    }

    /**
     * 删除款单
     * @param id 款单编号
     * @return 1：成功
     */
    public int delete(String id){
        return mapper.deleteByPrimaryKey(id);
    }

    /**
     * 更新款单
     * @param receipt 款单实体
     * @return 1：成功
     */
    public int update(Receipt receipt){
        return mapper.updateByPrimaryKeySelective(receipt);
    }

    /**
     * 确认款单
     * @param receipt 款单实体
     * @return 1：成功
     */
    public int confirm(Receipt receipt){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String s = dateFormat.format(new Date());
        receipt.setConfirmationDate(s);
        int update = mapper.updateByPrimaryKeySelective(receipt);
        int add = expenseService.add(receipt);
        return add==1 && update==1?1:-1;
    }

    /**
     * 通过ID查询款单
     * @param id 编号
     * @return
     */
    public Receipt queryOne(String id){
        return mapper.selectByPrimaryKey(id);
    }

    /**
     * 分页
     * 搜索符合条件的款单
     * @param search 往来客户
     * @param category 类型（付款单、收款单、其他收入单、其他支出单、其它应付款单、其它应收款单）
     * @param confirmStatus 确认状态（全部、已确认、未确认）
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param currentPage 当前页号
     * @param pageSize 页尺寸
     * @return List<Receipt>
     */
    public List<Receipt> queryAll(String search, String category, String confirmStatus,String beginTime, String endTime, int currentPage, int pageSize){
        RowBounds rowBunds = new RowBounds((currentPage - 1) * pageSize, pageSize);
        Example example=new Example(Receipt.class);
        example.setOrderByClause("payment_date desc");
        Example.Criteria criteria = example.createCriteria();
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("transitUnit","%"+search+"%");
        }
        if(!"全部".equals(category)){
            criteria.andEqualTo("category",category);
        }
        if(!"全部".equals(confirmStatus)){
            criteria.andEqualTo("confirmationStatus",confirmStatus);
        }
        if(StringUtils.isNotEmpty(beginTime)){
            criteria.andGreaterThanOrEqualTo("paymentDate",beginTime);
        }
        if(StringUtils.isNotEmpty(endTime)){
            criteria.andLessThanOrEqualTo("paymentDate",endTime);
        }
        return mapper.selectByExampleAndRowBounds(example,rowBunds);
    }

    /**
     * 分页
     * 搜索符合条件的款单的数量
     * @param search 往来客户
     * @param category 类型（付款单、收款单、其他收入单、其他支出单、其它应付款单、其它应收款单）
     * @param confirmStatus 确认状态（全部、已确认）
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @return 数量
     */
    public int queryAll(String search, String category, String confirmStatus,String beginTime, String endTime){
        Example example=new Example(Receipt.class);
        example.setOrderByClause("payment_date desc");
        Example.Criteria criteria = example.createCriteria();
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("transitUnit","%"+search+"%");
        }
        if(!"全部".equals(category)){
            criteria.andEqualTo("category",category);
        }
        if(!"全部".equals(category)){
            criteria.andEqualTo("confirmationStatus",confirmStatus);
        }
        if(StringUtils.isNotEmpty(beginTime)){
            criteria.andGreaterThanOrEqualTo("paymentDate",beginTime);
        }
        if(StringUtils.isNotEmpty(endTime)){
            criteria.andLessThanOrEqualTo("paymentDate",endTime);
        }
        return mapper.selectCountByExample(example);
    }

}
