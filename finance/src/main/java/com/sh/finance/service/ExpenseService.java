package com.sh.finance.service;

import com.sh.finance.entity.Expense;
import com.sh.finance.entity.Receipt;
import com.sh.finance.mapper.IExpenseMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Author: he changjie on 2019/3/8
 * @Description:
 * 报表管理
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ExpenseService {
    @Autowired
    private IExpenseMapper mapper;

    /**
     * 新增一条汇总表记录
     * @param receipt 出纳信息
     * @return 1：成功
     */
    int add(Receipt receipt){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = sdf.format(new Date());
        Expense expense = new Expense();
        expense.setExpenseDate(receipt.getPaymentDate());
        expense.setDocumentNumber(receipt.getReceiptNumber());
        expense.setCoding("记-2203");
        switch (receipt.getCategory()) {
            case "付款单":{
                expense.setCounterpartSubject("应收账款");
                expense.setAccountingSubject("应付账款");
                if(receipt.getSummary().contains("借")){
                    expense.setDebit(receipt.getPaymentAmount());
                    expense.setDirection("借");
                }
            }break;
            case "收款单":{
                expense.setCounterpartSubject("应付账款");
                expense.setAccountingSubject("应收账款");
                if(receipt.getSummary().contains("借")){
                    expense.setCredit(receipt.getPaymentAmount());
                    expense.setDirection("贷");
                }
            }break;
            case "其他收入单":
            case "其它应收款单":{
                expense.setCounterpartSubject("其他应付账款");
                expense.setAccountingSubject("其他应收账款");
                if(receipt.getSummary().contains("借")){
                    expense.setCredit(receipt.getPaymentAmount());
                    expense.setDirection("贷");
                }
            }break;
            case "其他支出单":
            case "其它应付款单":{
                expense.setCounterpartSubject("其他应收账款");
                expense.setAccountingSubject("其他应付账款");
                if(receipt.getSummary().contains("借")){
                    expense.setDebit(receipt.getPaymentAmount());
                    expense.setDirection("借");
                }
            }break;
            default:break;
        }
        expense.setSummary(receipt.getSummary());
        expense.setTransitUnit(receipt.getTransitUnit());
        Example example = new Example(Expense.class);
        example.setOrderByClause("create_time desc");
        List<Expense> lists = mapper.selectByExample(example);
        double balance=0.0d;
        if(lists!=null && lists.size()>0){
            balance=Double.parseDouble(lists.get(0).getBalance());
        }
        balance+=Double.parseDouble(receipt.getPaymentAmount());
        String s = String.valueOf(balance);
        int len=0;
        if(s.lastIndexOf(".")!=-1){
            int i=3;
            while(true){
                if(!((s.lastIndexOf(".")+i)>s.length())){
                    len=i;
                    break;
                }else{
                    i--;
                }
            }
        }else{
            len=s.length();
        }
        expense.setBalance(s.substring(0,len));
        expense.setCreateTime(time);
        return mapper.insertSelective(expense);
    }

    /**
     * 分页
     * 搜索符合条件的汇总表
     * @param search 客户名称
     * @param category 会计科目(全部、应收账款、应付账款、其他应收账款、其他应付账款)
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param currentPage 当前页号
     * @param pageSize 页尺寸
     * @return List<Expense>
     */
    public List<Expense> queryAll(String search,String category,String beginTime,String endTime,int currentPage,int pageSize){
        RowBounds rowBunds = new RowBounds((currentPage - 1) * pageSize, pageSize);
        Example example=new Example(Expense.class);
        example.setOrderByClause("expense_date desc");
        Example.Criteria criteria = example.createCriteria();
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("transitUnit","%"+search+"%");
        }
        if(StringUtils.isNotEmpty(category) && !"全部".equals(category)){
            criteria.andEqualTo("accountingSubject",category);
        }
        if(StringUtils.isNotEmpty(beginTime)){
            criteria.andGreaterThanOrEqualTo("expenseDate",beginTime);
        }
        if(StringUtils.isNotEmpty(endTime)){
            criteria.andLessThanOrEqualTo("expenseDate",endTime);
        }
        return mapper.selectByExampleAndRowBounds(example,rowBunds);
    }

    /**
     * 分页
     * 搜索符合条件的汇总表的数量
     * @param search 客户名称
     * @param category 会计科目(全部、应收账款、应付账款、其他应收账款、其他应付账款)
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @return 数量
     */
    public int queryAll(String search,String category,String beginTime,String endTime){
        Example example=new Example(Expense.class);
        example.setOrderByClause("expense_date desc");
        Example.Criteria criteria = example.createCriteria();
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("transitUnit","%"+search+"%");
        }
        if(StringUtils.isNotEmpty(category) && !"全部".equals(category)){
            criteria.andEqualTo("accountingSubject",category);
        }
        if(StringUtils.isNotEmpty(beginTime)){
            criteria.andGreaterThanOrEqualTo("expenseDate",beginTime);
        }
        if(StringUtils.isNotEmpty(endTime)){
            criteria.andLessThanOrEqualTo("expenseDate",endTime);
        }
        return mapper.selectCountByExample(example);
    }
}
