package com.sh.finance.service;

import com.sh.finance.entity.GoodsRecord;
import com.sh.finance.mapper.IGoodsRecordMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Author: he changjie on 2019/3/8
 * @Description:
 * 库存记录表
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class GoodsRecordService {
    @Autowired
    private IGoodsRecordMapper mapper;
    @Autowired
    private GoodsService goodsService;

    /**
     * 货物出库
     * @param record 记录
     * @return 1：成功
     */
    public int outBound(GoodsRecord record){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = sdf.format(new Date());
        record.setOperateTime(time);
        record.setCategory("出库");
        goodsService.less(record.getGoodsId(),record.getNumber());
        return mapper.insertSelective(record);
    }

    /**
     * 货物入库
     * @param record 记录
     * @return 1：成功
     */
    public int inBound(GoodsRecord record){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = sdf.format(new Date());
        record.setOperateTime(time);
        record.setCategory("入库");
        return mapper.insertSelective(record);
    }

    /**
     * 通过ID查询货物出入库记录
     * @param id 记录ID
     * @return 出入库记录
     */
    public GoodsRecord queryOne(String id){
        return mapper.selectByPrimaryKey(id);
    }

    /**
     * 分页
     * 搜索符合条件的入库信息
     * @param goodsName 货物名称
     * @param clientName 客户名称
     * @param category 操作类型（入库、出库）
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param currentPage 当前页号
     * @param pageSize 页尺寸
     * @return
     */
    public List<GoodsRecord> queryAll(String goodsName, String clientName, String category, String beginTime, String endTime, int currentPage, int pageSize){
        RowBounds rowBunds = new RowBounds((currentPage - 1) * pageSize, pageSize);
        Example example=new Example(GoodsRecord.class);
        example.setOrderByClause("operate_time desc");
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("category",category);
        if(StringUtils.isNotEmpty(goodsName)){
            criteria.andLike("goodsName","%"+goodsName+"%");
        }
        if(StringUtils.isNotEmpty(clientName)){
            criteria.andLike("clientName","%"+clientName+"%");
        }
        if(StringUtils.isNotEmpty(beginTime)){
            criteria.andGreaterThanOrEqualTo("operateTime",beginTime);
        }
        if(StringUtils.isNotEmpty(endTime)){
            criteria.andLessThanOrEqualTo("operateTime",endTime);
        }
        return mapper.selectByExampleAndRowBounds(example,rowBunds);
    }

    /**
     * 分页
     * 搜索符合条件的入库信息的数量
     * @param goodsName 货物名称
     * @param clientName 客户名称
     * @param category 操作类型（入库、出库）
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @return 数量
     */
    public int queryAll(String goodsName, String clientName, String category, String beginTime, String endTime){
        Example example=new Example(GoodsRecord.class);
        example.setOrderByClause("operate_time desc");
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("category",category);
        if(StringUtils.isNotEmpty(goodsName)){
            criteria.andLike("goodsName","%"+goodsName+"%");
        }
        if(StringUtils.isNotEmpty(clientName)){
            criteria.andLike("clientName","%"+clientName+"%");
        }
        if(StringUtils.isNotEmpty(beginTime)){
            criteria.andGreaterThanOrEqualTo("operateTime",beginTime);
        }
        if(StringUtils.isNotEmpty(endTime)){
            criteria.andLessThanOrEqualTo("operateTime",endTime);
        }
        return mapper.selectCountByExample(example);
    }
}
