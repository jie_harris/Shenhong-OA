package com.sh.finance.service;

import com.sh.finance.entity.Goods;
import com.sh.finance.entity.Project;
import com.sh.finance.mapper.IProjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @Author: he changjie on 2019/3/8
 * @Description:
 * 项目管理
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ProjectService {
    @Autowired
    private IProjectMapper mapper;

    /**
     * 添加一个项目
     * @param project 项目实体
     * @return 1：成功
     */
    public int add(Project project){
        project.setStatus("进行中");
        return mapper.insertSelective(project);
    }

    /**
     * 删除一个项目
     * @param id ID
     * @return 1：成功
     */
    public int delete(String id){
        Project one = queryOne(id);
        one.setFlag("-1");
        return update(one);
    }

    /**
     * 更新一个项目
     * @param project 项目实体
     * @return 1：成功
     */
    public int update(Project project){
        return mapper.updateByPrimaryKeySelective(project);
    }

    /**
     * 核算一个项目
     * @param project 项目实体
     * @return 1：成功
     */
    public int account(Project project){
        project.setStatus("完结");
        return update(project);
    }

    /**
     * 通过ID拉取一个项目
     * @param id ID
     * @return 项目实体
     */
    public Project queryOne(String id){
        return mapper.selectByPrimaryKey(id);
    }

    /**
     * 分页
     * 搜索符合条件的项目记录
     * @param search 项目名称
     * @param currentPage 当前页号
     * @param pageSize 页尺寸
     * @return List<Project>
     */
    public List<Project> queryAll(String search,int currentPage, int pageSize){
        RowBounds rowBunds = new RowBounds((currentPage - 1) * pageSize, pageSize);
        Example example=new Example(Project.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andIsNull("flag");
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("projectName","%"+search+"%");
        }
        return mapper.selectByExampleAndRowBounds(example,rowBunds);
    }

    /**
     * 分页
     * 搜索符合条件的项目记录的数量
     * @param search 项目名称
     * @return 数量
     */
    public int queryAll(String search){
        Example example=new Example(Project.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andIsNull("flag");
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("projectName","%"+search+"%");
        }
        return mapper.selectCountByExample(example);
    }


}
