package com.sh.finance.controller;

import com.google.common.collect.Maps;
import com.sh.finance.common.JsonMapper;
import com.sh.finance.common.ResultUtil;
import com.sh.finance.entity.*;
import com.sh.finance.enums.ResultEnum;
import com.sh.finance.handle.ExceptionHandle;
import com.sh.finance.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: he changjie on 2019/3/9
 * @Description:
 */
@RestController
@RequestMapping("/finance")
public class FinanceController {
    @Autowired
    private ExceptionHandle exceptionHandle;
    private JsonMapper jsonMapper = JsonMapper.INSTANCE;
    @Autowired
    private ExpenseService expenseService;
    @Autowired
    private ReceiptService receiptService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private GoodsRecordService recordService;
    @Autowired
    private WagesService wagesService;
    @Autowired
    private ProjectService projectService;

    //********************************* 资金 *********************************
    /**
     * 新增一个款单
     * @param receipt 款单实体
     * @return ResponseEntity
     */
    @RequestMapping(value = "/receipt/add",method = RequestMethod.POST)
    public ResponseEntity addReceipt(@RequestBody Receipt receipt){
        int add = receiptService.add(receipt);
        try {
            if(add!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 删除款单
     * @param id 款单编号
     * @return ResponseEntity
     */
    @RequestMapping(value = "/receipt/delete/{id}",method = RequestMethod.GET)
    public ResponseEntity deleteReceipt(@PathVariable String id){
        int delete = receiptService.delete(id);
        try {
            if(delete!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 更新款单
     * @param receipt 款单实体
     * @return
     */
    @RequestMapping(value = "/receipt/update",method = RequestMethod.POST)
    public ResponseEntity updateReceipt(@RequestBody Receipt receipt){
        int update = receiptService.update(receipt);
        try {
            if(update!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 确认款单
     * @param receipt 款单实体
     * @return
     */
    @RequestMapping(value = "/receipt/confirm",method = RequestMethod.POST)
    public ResponseEntity confirmReceipt(@RequestBody Receipt receipt){
        int confirm = receiptService.confirm(receipt);
        try {
            if(confirm!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 通过ID查询款单
     * @param id 编号
     * @return ResponseEntity
     */
    @RequestMapping(value = "/receipt/queryOne/{id}",method = RequestMethod.GET)
    public ResponseEntity queryOneReceipt(@PathVariable String id){
        Receipt queryOne = receiptService.queryOne(id);
        try {
            if(queryOne!=null){
                return new ResponseEntity(ResultUtil.success(queryOne), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 分页
     * 搜索符合条件的款单
     * @param params 搜索条件
     * @return ResponseEntity
     */
    @RequestMapping(value = "/receipt/queryAll/{params}",method = RequestMethod.GET)
    public ResponseEntity queryAllReceipt(@PathVariable String params){
        try {
            Map fromJson = jsonMapper.fromJson(params, Map.class);
            String search = (String)fromJson.get("search");
            String category = (String)fromJson.get("category");
            String confirmStatus = (String)fromJson.get("confirmStatus");
            String beginTime = (String)fromJson.get("beginTime");
            String endTime = (String)fromJson.get("endTime");
            int currentPage = (int)fromJson.get("currentPage");
            int pageSize = (int)fromJson.get("pageSize");
            List<Receipt> list = receiptService.queryAll(search,category,confirmStatus,beginTime,endTime,currentPage,pageSize);
            if(list!=null){
                HashMap<String, Object> map = Maps.newHashMap();
                map.put("list",list);
                map.put("total",receiptService.queryAll(search,category,confirmStatus,beginTime,endTime));
                map.put("currentPage",currentPage);
                map.put("pageSize",pageSize);
                return new ResponseEntity(ResultUtil.success(map), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    //********************************* 报表管理 *********************************

    /**
     * 分页
     * 搜索符合条件的汇总表
     * @param params 搜索条件
     * @return ResponseEntity
     */
    @RequestMapping(value = "/expense/queryAll/{params}",method = RequestMethod.GET)
    public ResponseEntity queryAllExpense(@PathVariable String params){
        try {
            Map fromJson = jsonMapper.fromJson(params, Map.class);
            String search = (String)fromJson.get("search");
            String category = (String)fromJson.get("category");
            String beginTime = (String)fromJson.get("beginTime");
            String endTime = (String)fromJson.get("endTime");
            int currentPage = (int)fromJson.get("currentPage");
            int pageSize = (int)fromJson.get("pageSize");
            List<Expense> list = expenseService.queryAll(search,category,beginTime,endTime,currentPage,pageSize);
            if(list!=null){
                HashMap<String, Object> map = Maps.newHashMap();
                map.put("list",list);
                map.put("total",expenseService.queryAll(search,category,beginTime,endTime));
                map.put("currentPage",currentPage);
                map.put("pageSize",pageSize);
                return new ResponseEntity(ResultUtil.success(map), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    //********************************* 库存管理 *********************************

    /**
     * 新增货物入库，若已存在的货物则数量增加
     * @param goods 货物实体
     * @return ResponseEntity
     */
    @RequestMapping(value = "/goods/add",method = RequestMethod.POST)
    public ResponseEntity addGoods(@RequestBody Goods goods){
        int add = goodsService.add(goods);
        try {
            if(add!=-1){
                return new ResponseEntity(ResultUtil.success(goods.getId()), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 货物出库
     * @param id ID
     * @param num 数量
     * @return ResponseEntity
     */
    @RequestMapping(value = "/goods/less/{id}/{num}",method = RequestMethod.GET)
    public ResponseEntity lessGoods(@PathVariable String id,@PathVariable String num){
        int less = goodsService.less(id,num);
        try {
            if(less!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 删除货物
     * @param id 货物编号
     * @return ResponseEntity
     */
    @RequestMapping(value = "/goods/delete/{id}",method = RequestMethod.GET)
    public ResponseEntity deleteGoods(@PathVariable String id){
        int delete = goodsService.delete(id);
        try {
            if(delete!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 更新货物
     * @param goods 货物实体
     * @return
     */
    @RequestMapping(value = "/goods/update",method = RequestMethod.POST)
    public ResponseEntity updateGoods(@RequestBody Goods goods){
        int update = goodsService.update(goods);
        try {
            if(update!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 通过ID查询货物
     * @param id 编号
     * @return ResponseEntity
     */
    @RequestMapping(value = "/goods/queryOne/{id}",method = RequestMethod.GET)
    public ResponseEntity queryOneGoods(@PathVariable String id){
        Goods queryOne = goodsService.queryOne(id);
        try {
            if(queryOne!=null){
                return new ResponseEntity(ResultUtil.success(queryOne), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 分页
     * 搜索符合条件的货物
     * @param params 搜索条件
     * @return ResponseEntity
     */
    @RequestMapping(value = "/goods/queryAll/{params}",method = RequestMethod.GET)
    public ResponseEntity queryAllGoods(@PathVariable String params){
        try {
            Map fromJson = jsonMapper.fromJson(params, Map.class);
            String goodsName = (String)fromJson.get("goodsName");
            int currentPage = (int)fromJson.get("currentPage");
            int pageSize = (int)fromJson.get("pageSize");
            List<Goods> list = goodsService.queryAll(goodsName,currentPage,pageSize);
            if(list!=null){
                HashMap<String, Object> map = Maps.newHashMap();
                map.put("list",list);
                map.put("total",goodsService.queryAll(goodsName));
                map.put("currentPage",currentPage);
                map.put("pageSize",pageSize);
                return new ResponseEntity(ResultUtil.success(map), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 新增货物出库记录
     * @param record 记录
     * @return ResponseEntity
     */
    @RequestMapping(value = "/record/outbound",method = RequestMethod.POST)
    public ResponseEntity outbound(@RequestBody GoodsRecord record){
        int add = recordService.outBound(record);
        try {
            if(add!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 新增货物入库记录
     * @param record 记录
     * @return ResponseEntity
     */
    @RequestMapping(value = "/record/inbound",method = RequestMethod.POST)
    public ResponseEntity inbound(@RequestBody GoodsRecord record){
        try {
            int add = recordService.inBound(record);
            if(add!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 通过ID查询货物出入库记录
     * @param id 记录ID
     * @return ResponseEntity
     */
    @RequestMapping(value = "/record/queryOne/{id}",method = RequestMethod.GET)
    public ResponseEntity queryOneGoodsRecord(@PathVariable String id){
        GoodsRecord queryOne = recordService.queryOne(id);
        try {
            if(queryOne!=null){
                return new ResponseEntity(ResultUtil.success(queryOne), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 分页
     * 搜索符合条件的入库信息
     * @param params 搜索条件
     * @return ResponseEntity
     */
    @RequestMapping(value = "/record/queryAll/{params}",method = RequestMethod.GET)
    public ResponseEntity queryAllGoodsRecord(@PathVariable String params){
        try {
            Map fromJson = jsonMapper.fromJson(params, Map.class);
            String goodsName = (String)fromJson.get("goodsName");
            String clientName = (String)fromJson.get("clientName");
            String category = (String)fromJson.get("category");
            String beginTime = (String)fromJson.get("beginTime");
            String endTime = (String)fromJson.get("endTime");
            int currentPage = (int)fromJson.get("currentPage");
            int pageSize = (int)fromJson.get("pageSize");
            List<GoodsRecord> list = recordService.queryAll(goodsName,clientName,category,beginTime,endTime,currentPage,pageSize);
            if(list!=null){
                HashMap<String, Object> map = Maps.newHashMap();
                map.put("list",list);
                map.put("total",recordService.queryAll(goodsName,clientName,category,beginTime,endTime));
                map.put("currentPage",currentPage);
                map.put("pageSize",pageSize);
                return new ResponseEntity(ResultUtil.success(map), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    //********************************* 项目管理 *********************************
    /**
     * 添加一个项目
     * @param project 项目实体
     * @return ResponseEntity
     */
    @RequestMapping(value = "/project/add",method = RequestMethod.POST)
    public ResponseEntity addProject(@RequestBody Project project){
        int add = projectService.add(project);
        try {
            if(add!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 删除项目记录
     * @param id 项目记录编号
     * @return ResponseEntity
     */
    @RequestMapping(value = "/project/delete/{id}",method = RequestMethod.GET)
    public ResponseEntity deleteProject(@PathVariable String id){
        int delete = projectService.delete(id);
        try {
            if(delete!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 核算一个项目
     * @param project 项目实体
     * @return
     */
    @RequestMapping(value = "/project/account",method = RequestMethod.POST)
    public ResponseEntity accountProject(@RequestBody Project project){
        int account = projectService.account(project);
        try {
            if(account!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 更新项目记录
     * @param project 货物实体
     * @return
     */
    @RequestMapping(value = "/project/update",method = RequestMethod.POST)
    public ResponseEntity updateProject(@RequestBody Project project){
        int update = projectService.update(project);
        try {
            if(update!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 通过ID查询项目记录
     * @param id 编号
     * @return ResponseEntity
     */
    @RequestMapping(value = "/project/queryOne/{id}",method = RequestMethod.GET)
    public ResponseEntity queryOneProject(@PathVariable String id){
        Project project = projectService.queryOne(id);
        try {
            if(project!=null){
                return new ResponseEntity(ResultUtil.success(project), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 分页
     * 搜索符合条件的项目记录
     * @param params 搜索条件
     * @return ResponseEntity
     */
    @RequestMapping(value = "/project/queryAll/{params}",method = RequestMethod.GET)
    public ResponseEntity queryAllProject(@PathVariable String params){
        try {
            Map fromJson = jsonMapper.fromJson(params, Map.class);
            String search = (String)fromJson.get("search");
            int currentPage = (int)fromJson.get("currentPage");
            int pageSize = (int)fromJson.get("pageSize");
            List<Project> list = projectService.queryAll(search,currentPage,pageSize);
            if(list!=null){
                HashMap<String, Object> map = Maps.newHashMap();
                map.put("list",list);
                map.put("total",projectService.queryAll(search));
                map.put("currentPage",currentPage);
                map.put("pageSize",pageSize);
                return new ResponseEntity(ResultUtil.success(map), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    //********************************* 工作核算 *********************************
    /**
     * 添加一个工作核算记录
     * @param wages 工作核算记录实体
     * @return ResponseEntity
     */
    @RequestMapping(value = "/wages/add",method = RequestMethod.POST)
    public ResponseEntity addProject(@RequestBody Wages wages){
        int add = wagesService.add(wages);
        try {
            if(add!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 更新工作核算记录
     * @param wages 工作核算记录实体
     * @return
     */
    @RequestMapping(value = "/wages/update",method = RequestMethod.POST)
    public ResponseEntity updateWages(@RequestBody Wages wages){
        int update = wagesService.update(wages);
        try {
            if(update!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 通过ID查询工作核算记录
     * @param id 编号
     * @return ResponseEntity
     */
    @RequestMapping(value = "/wages/queryOne/{id}",method = RequestMethod.GET)
    public ResponseEntity queryOneWages(@PathVariable String id){
        Wages wages = wagesService.queryOne(id);
        try {
            if(wages!=null){
                return new ResponseEntity(ResultUtil.success(wages), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 通过员工ID和工作期数获取固定工资和计件说明
     * @param params 搜索条件
     * @return
     */
    @RequestMapping(value = "/wages/money/{params}",method = RequestMethod.GET)
    public ResponseEntity queryAllWagesMoney(@PathVariable String params){
        try {
            Map fromJson = jsonMapper.fromJson(params, Map.class);
            String id = (String)fromJson.get("id");
            String time = (String)fromJson.get("time");
            Map<String, String> stringStringMap = wagesService.queryMoney(id, time);
            if(stringStringMap!=null){
                return new ResponseEntity(ResultUtil.success(stringStringMap), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 分页
     * 搜索符合条件的工作核算记录
     * @param params 搜索条件
     * @return ResponseEntity
     */
    @RequestMapping(value = "/wages/queryAll/{params}",method = RequestMethod.GET)
    public ResponseEntity queryAllWages(@PathVariable String params){
        try {
            Map fromJson = jsonMapper.fromJson(params, Map.class);
            String search = (String)fromJson.get("search");
            int currentPage = (int)fromJson.get("currentPage");
            int pageSize = (int)fromJson.get("pageSize");
            List<Wages> list = wagesService.queryAll(search,currentPage,pageSize);
            if(list!=null){
                HashMap<String, Object> map = Maps.newHashMap();
                map.put("list",list);
                map.put("total",wagesService.queryAll(search));
                map.put("currentPage",currentPage);
                map.put("pageSize",pageSize);
                return new ResponseEntity(ResultUtil.success(map), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }
}
