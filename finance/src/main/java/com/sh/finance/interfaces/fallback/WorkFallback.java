package com.sh.finance.interfaces.fallback;

import com.sh.finance.common.Result;
import com.sh.finance.entity.WorkPlan;
import com.sh.finance.interfaces.WorkInterface;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: he changjie on 2019/3/10
 * @Description:
 */
@Component
public class WorkFallback implements WorkInterface {
    @Override
    public Result<List<WorkPlan>> queryAllS(String params) {
        return null;
    }
}
