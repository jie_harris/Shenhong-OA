package com.sh.finance.interfaces.fallback;

import com.sh.finance.common.Result;
import com.sh.finance.entity.Payment;
import com.sh.finance.interfaces.PaymentInterface;
import org.springframework.stereotype.Component;

/**
 * @Author: he changjie on 2019/3/10
 * @Description:
 */
@Component
public class PaymentFallback implements PaymentInterface {

    @Override
    public Result<Payment> queryOne(String id) {
        return null;
    }
}
