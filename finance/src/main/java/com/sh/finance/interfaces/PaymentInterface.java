package com.sh.finance.interfaces;

import com.sh.finance.common.Result;
import com.sh.finance.entity.Payment;
import com.sh.finance.interfaces.fallback.PaymentFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Author: he changjie on 2019/3/10
 * @Description:
 */
@Component
@FeignClient(name = "human-resources",fallback = PaymentFallback.class)
public interface PaymentInterface {
    /**
     * 通过ID查询薪酬信息
     * @param id ID
     * @return List<Payment>
     */
    @RequestMapping(value = "/hr/payment/queryOneByNo/{id}",method = RequestMethod.GET)
    Result<Payment> queryOne(@PathVariable("id") String id);
}
