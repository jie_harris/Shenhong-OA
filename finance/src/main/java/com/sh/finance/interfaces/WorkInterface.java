package com.sh.finance.interfaces;

import com.sh.finance.common.Result;
import com.sh.finance.entity.WorkPlan;
import com.sh.finance.interfaces.fallback.WorkFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @Author: he changjie on 2019/3/10
 * @Description:
 */
@Component
@FeignClient(name = "work-plan",fallback = WorkFallback.class)
public interface WorkInterface {
    /**
     * 通过员工ID查询该期间内有无完成的工作
     * @param params 搜索参数
     * @return List<WorkPlan>
     */
    @RequestMapping(value = "/wp/queryAllW/{params}",method = RequestMethod.GET)
    Result<List<WorkPlan>> queryAllS(@PathVariable("params") String params);
}
