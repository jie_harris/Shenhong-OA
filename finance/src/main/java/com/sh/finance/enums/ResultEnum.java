package com.sh.finance.enums;

/**
 * 接口返回处理状态信息枚举类
 * @author KeShi
 */
public enum ResultEnum {

    _UNKNOWN_SERVER_ERROR(-2, "未知错误"),
    _SERVER_EXCEPTION(-3, "系统异常"),
    _SUCCESS(0, "执行成功"),
    _FAILED(-1, "执行失败");


    private int enumCode;
    private String enumMsg;

    ResultEnum(int code, String msg){
        this.enumCode = code;
        this.enumMsg = msg;
    }

    public int getEnumCode() {
        return enumCode;
    }

    public void setEnumCode(int enumCode) {
        this.enumCode = enumCode;
    }

    public String getEnumMsg() {
        return enumMsg;
    }

    public void setEnumMsg(String enumMsg) {
        this.enumMsg = enumMsg;
    }
}
