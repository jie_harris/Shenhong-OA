package com.sh.finance.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "shoa_wages")
public class Wages {
  @Id
  @GeneratedValue(generator = "UUID")
  private String id;
  private String staffName;
  private String fixedSalary;
  private String userId;
  private String periods;
  private String deductionAmount;
  private String deductionDesc;
  private String pieceAmount;
  private String pieceDesc;
  private String subsidy;
  private String subsidyDesc;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getStaffName() {
    return staffName;
  }

  public void setStaffName(String staffName) {
    this.staffName = staffName;
  }


  public String getFixedSalary() {
    return fixedSalary;
  }

  public void setFixedSalary(String fixedSalary) {
    this.fixedSalary = fixedSalary;
  }


  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }


  public String getPeriods() {
    return periods;
  }

  public void setPeriods(String periods) {
    this.periods = periods;
  }


  public String getDeductionAmount() {
    return deductionAmount;
  }

  public void setDeductionAmount(String deductionAmount) {
    this.deductionAmount = deductionAmount;
  }


  public String getDeductionDesc() {
    return deductionDesc;
  }

  public void setDeductionDesc(String deductionDesc) {
    this.deductionDesc = deductionDesc;
  }


  public String getPieceAmount() {
    return pieceAmount;
  }

  public void setPieceAmount(String pieceAmount) {
    this.pieceAmount = pieceAmount;
  }


  public String getPieceDesc() {
    return pieceDesc;
  }

  public void setPieceDesc(String pieceDesc) {
    this.pieceDesc = pieceDesc;
  }


  public String getSubsidy() {
    return subsidy;
  }

  public void setSubsidy(String subsidy) {
    this.subsidy = subsidy;
  }


  public String getSubsidyDesc() {
    return subsidyDesc;
  }

  public void setSubsidyDesc(String subsidyDesc) {
    this.subsidyDesc = subsidyDesc;
  }

}
