package com.sh.finance.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "shoa_project")
public class Project {
    @Id
    @GeneratedValue(generator = "UUID")
  private String id;
  private String contractNo;
  private String projectName;
  private String projectDate;
  private String projectAddress;
  private String companyName;
  private String status;
  private String projectDesc;
  private String price;
  private String estimatedExpenses;
  private String totalExpenses;
  private String expensesDesc;
  private String accountantId;
  private String accountant;
  private String imgPath;
  private String flag;

  public String getFlag() {
    return flag;
  }

  public void setFlag(String flag) {
    this.flag = flag;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getContractNo() {
    return contractNo;
  }

  public void setContractNo(String contractNo) {
    this.contractNo = contractNo;
  }


  public String getProjectName() {
    return projectName;
  }

  public void setProjectName(String projectName) {
    this.projectName = projectName;
  }


  public String getProjectDate() {
    return projectDate;
  }

  public void setProjectDate(String projectDate) {
    this.projectDate = projectDate;
  }


  public String getProjectAddress() {
    return projectAddress;
  }

  public void setProjectAddress(String projectAddress) {
    this.projectAddress = projectAddress;
  }


  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }


  public String getProjectDesc() {
    return projectDesc;
  }

  public void setProjectDesc(String projectDesc) {
    this.projectDesc = projectDesc;
  }


  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }


  public String getEstimatedExpenses() {
    return estimatedExpenses;
  }

  public void setEstimatedExpenses(String estimatedExpenses) {
    this.estimatedExpenses = estimatedExpenses;
  }


  public String getTotalExpenses() {
    return totalExpenses;
  }

  public void setTotalExpenses(String totalExpenses) {
    this.totalExpenses = totalExpenses;
  }


  public String getExpensesDesc() {
    return expensesDesc;
  }

  public void setExpensesDesc(String expensesDesc) {
    this.expensesDesc = expensesDesc;
  }


  public String getAccountantId() {
    return accountantId;
  }

  public void setAccountantId(String accountantId) {
    this.accountantId = accountantId;
  }


  public String getAccountant() {
    return accountant;
  }

  public void setAccountant(String accountant) {
    this.accountant = accountant;
  }


  public String getImgPath() {
    return imgPath;
  }

  public void setImgPath(String imgPath) {
    this.imgPath = imgPath;
  }

}
