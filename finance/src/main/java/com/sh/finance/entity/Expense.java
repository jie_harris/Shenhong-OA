package com.sh.finance.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "shoa_expense_account")
public class Expense {
  @Id
  @GeneratedValue(generator = "UUID")
  private String id;
  private String expenseDate;
  private String documentNumber;
  private String coding;
  private String accountingSubject;
  private String counterpartSubject;
  private String summary;
  private String debit;
  private String credit;
  private String transitUnit;
  private String direction;
  private String balance;
  private String createTime;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getExpenseDate() {
    return expenseDate;
  }

  public void setExpenseDate(String expenseDate) {
    this.expenseDate = expenseDate;
  }


  public String getDocumentNumber() {
    return documentNumber;
  }

  public void setDocumentNumber(String documentNumber) {
    this.documentNumber = documentNumber;
  }


  public String getCoding() {
    return coding;
  }

  public void setCoding(String coding) {
    this.coding = coding;
  }


  public String getAccountingSubject() {
    return accountingSubject;
  }

  public void setAccountingSubject(String accountingSubject) {
    this.accountingSubject = accountingSubject;
  }


  public String getCounterpartSubject() {
    return counterpartSubject;
  }

  public void setCounterpartSubject(String counterpartSubject) {
    this.counterpartSubject = counterpartSubject;
  }


  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }


  public String getDebit() {
    return debit;
  }

  public void setDebit(String debit) {
    this.debit = debit;
  }


  public String getCredit() {
    return credit;
  }

  public void setCredit(String credit) {
    this.credit = credit;
  }


  public String getTransitUnit() {
    return transitUnit;
  }

  public void setTransitUnit(String transitUnit) {
    this.transitUnit = transitUnit;
  }


  public String getDirection() {
    return direction;
  }

  public void setDirection(String direction) {
    this.direction = direction;
  }


  public String getBalance() {
    return balance;
  }

  public void setBalance(String balance) {
    this.balance = balance;
  }


  public String getCreateTime() {
    return createTime;
  }

  public void setCreateTime(String createTime) {
    this.createTime = createTime;
  }

}
