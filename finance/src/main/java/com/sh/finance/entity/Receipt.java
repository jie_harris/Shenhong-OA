package com.sh.finance.entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "shoa_receipt")
public class Receipt {
    @Id
    @GeneratedValue(generator = "UUID")
  private String id;
  private String category;
  private String paymentDate;
  private String receiptNumber;
  private String transitUnit;
  private String paymentAmount;
  private String paymentMethod;
  private String bank;
  private String bankAccount;
  private String executive;
  private String exchangeRate;
  private String belongProject;
  private String summary;
  private String confirmationStatus;
  private String confirmationDate;
  private String confirmationPeopleId;
  private String confirmationPeople;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }


  public String getPaymentDate() {
    return paymentDate;
  }

  public void setPaymentDate(String paymentDate) {
    this.paymentDate = paymentDate;
  }


  public String getReceiptNumber() {
    return receiptNumber;
  }

  public void setReceiptNumber(String receiptNumber) {
    this.receiptNumber = receiptNumber;
  }


  public String getTransitUnit() {
    return transitUnit;
  }

  public void setTransitUnit(String transitUnit) {
    this.transitUnit = transitUnit;
  }


  public String getPaymentAmount() {
    return paymentAmount;
  }

  public void setPaymentAmount(String paymentAmount) {
    this.paymentAmount = paymentAmount;
  }


  public String getPaymentMethod() {
    return paymentMethod;
  }

  public void setPaymentMethod(String paymentMethod) {
    this.paymentMethod = paymentMethod;
  }


  public String getBank() {
    return bank;
  }

  public void setBank(String bank) {
    this.bank = bank;
  }


  public String getBankAccount() {
    return bankAccount;
  }

  public void setBankAccount(String bankAccount) {
    this.bankAccount = bankAccount;
  }


  public String getExecutive() {
    return executive;
  }

  public void setExecutive(String executive) {
    this.executive = executive;
  }


  public String getExchangeRate() {
    return exchangeRate;
  }

  public void setExchangeRate(String exchangeRate) {
    this.exchangeRate = exchangeRate;
  }


  public String getBelongProject() {
    return belongProject;
  }

  public void setBelongProject(String belongProject) {
    this.belongProject = belongProject;
  }


  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }


  public String getConfirmationStatus() {
    return confirmationStatus;
  }

  public void setConfirmationStatus(String confirmationStatus) {
    this.confirmationStatus = confirmationStatus;
  }


  public String getConfirmationDate() {
    return confirmationDate;
  }

  public void setConfirmationDate(String confirmationDate) {
    this.confirmationDate = confirmationDate;
  }


  public String getConfirmationPeopleId() {
    return confirmationPeopleId;
  }

  public void setConfirmationPeopleId(String confirmationPeopleId) {
    this.confirmationPeopleId = confirmationPeopleId;
  }


  public String getConfirmationPeople() {
    return confirmationPeople;
  }

  public void setConfirmationPeople(String confirmationPeople) {
    this.confirmationPeople = confirmationPeople;
  }

}
