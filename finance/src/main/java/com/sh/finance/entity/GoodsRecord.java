package com.sh.finance.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "shoa_goods_record")
public class GoodsRecord {
  @Id
  @GeneratedValue(generator = "UUID")
  private String id;
  private String goodsId;
  private String number;
  private String clientName;
  private String deliverer;
  private String category;
  private String operaterId;
  private String operater;
  private String deliveryAddress;
  private String operateTime;
  private String summary;

  public String getId() {
    return id;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getGoodsId() {
    return goodsId;
  }

  public void setGoodsId(String goodsId) {
    this.goodsId = goodsId;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public String getClientName() {
    return clientName;
  }

  public void setClientName(String clientName) {
    this.clientName = clientName;
  }

  public String getDeliverer() {
    return deliverer;
  }

  public void setDeliverer(String deliverer) {
    this.deliverer = deliverer;
  }

  public String getOperaterId() {
    return operaterId;
  }

  public void setOperaterId(String operaterId) {
    this.operaterId = operaterId;
  }

  public String getOperater() {
    return operater;
  }

  public void setOperater(String operater) {
    this.operater = operater;
  }

  public String getDeliveryAddress() {
    return deliveryAddress;
  }

  public void setDeliveryAddress(String deliveryAddress) {
    this.deliveryAddress = deliveryAddress;
  }

  public String getOperateTime() {
    return operateTime;
  }

  public void setOperateTime(String operateTime) {
    this.operateTime = operateTime;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }
}
