package com.sh.finance.mapper;

import com.sh.finance.base.BaseMapper;
import com.sh.finance.entity.Goods;
import org.springframework.stereotype.Component;

/**
 * @Author: he changjie on 2019/3/8
 * @Description:
 */
@Component
public interface IGoodsMapper extends BaseMapper<Goods> {
}
