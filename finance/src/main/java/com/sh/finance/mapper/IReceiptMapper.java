package com.sh.finance.mapper;

import com.sh.finance.base.BaseMapper;
import com.sh.finance.entity.Receipt;
import org.springframework.stereotype.Component;

/**
 * @Author: he changjie on 2019/3/8
 * @Description:
 */
@Component
public interface IReceiptMapper extends BaseMapper<Receipt> {
}
