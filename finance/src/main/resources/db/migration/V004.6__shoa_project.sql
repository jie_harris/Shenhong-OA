/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : shoa-platform

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 24/01/2019 22:12:18
 项目管理
*/

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `shoa_project`;
CREATE TABLE `shoa_project`  (
  `id` varchar(32) NOT NULL,
  `contract_no` varchar(255) DEFAULT NULL COMMENT '合同编号',
  `project_name` varchar(255) DEFAULT NULL COMMENT '项目名称',
  `project_date` varchar(255) DEFAULT NULL COMMENT '项目时间',
  `project_address` varchar(255) DEFAULT NULL COMMENT '项目地址',
  `company_name` varchar(255) DEFAULT NULL COMMENT '合作方公司名称',
  `status` varchar(32) DEFAULT NULL COMMENT '项目情况（进行中、完结）',
  `project_desc` TEXT DEFAULT NULL COMMENT '项目说明',
  `price` varchar(255) DEFAULT NULL COMMENT '项目标价',
  `estimated_expenses` varchar(32) DEFAULT NULL COMMENT '项目预计支出',
  `total_expenses` varchar(32) DEFAULT NULL COMMENT '项目总支出',
  `expenses_desc` TEXT DEFAULT NULL COMMENT '项目支出说明',
  `accountant_id` varchar(32) DEFAULT NULL COMMENT '项目核算人ID',
  `accountant` varchar(255) DEFAULT NULL COMMENT '项目核算人',
  `img_path` varchar(255) DEFAULT NULL COMMENT '项目图片路径',
  `flag` varchar(3) DEFAULT NULL COMMENT '逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
