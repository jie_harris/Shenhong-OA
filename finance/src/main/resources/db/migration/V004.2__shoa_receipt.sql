/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : shoa-platform

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 24/01/2019 22:12:18
 付/收款单
*/

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `shoa_receipt`;
CREATE TABLE `shoa_receipt`  (
  `id` varchar(32) NOT NULL,
  `category` varchar(32) DEFAULT NULL COMMENT '类型（付款单、收款单、其他收入单、其他支出单、其它应付款单、其它应收款单）',
  `payment_date` varchar(32) DEFAULT NULL COMMENT '付款日期',
  `receipt_number` varchar(255) DEFAULT NULL COMMENT '发票号',
  `transit_unit` varchar(255) DEFAULT NULL COMMENT '往来单位',
  `payment_amount` varchar(255) DEFAULT NULL COMMENT '付款金额',
  `payment_method` varchar(32) DEFAULT NULL COMMENT '付款方式',
  `bank` varchar(255) DEFAULT NULL COMMENT '银行',
  `bank_account` varchar(255) DEFAULT NULL COMMENT '银行账号',
  `executive` varchar(255) DEFAULT NULL COMMENT '执行人',
  `exchange_rate` varchar(255) DEFAULT NULL COMMENT '汇率',
  `belong_project` varchar(255) DEFAULT NULL COMMENT '所属项目',
  `summary` varchar(255) DEFAULT NULL COMMENT '摘要',
  `confirmation_status` varchar(255) DEFAULT NULL COMMENT '确认状态(已确认、未确认)',
  `confirmation_date` varchar(32) DEFAULT NULL COMMENT '确认时间',
  `confirmation_people_id` varchar(32) DEFAULT NULL COMMENT '确认人ID',
  `confirmation_people` varchar(255) DEFAULT NULL COMMENT '确认人',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
