/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : shoa-platform

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 24/01/2019 22:12:18
 库存管理-出库单
*/

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `shoa_goods_record`;
CREATE TABLE `shoa_goods_record`  (
  `id` varchar(32) NOT NULL,
  `goods_id` varchar(32) NOT NULL COMMENT '货物ID，外键',
  `number` varchar(32) DEFAULT NULL COMMENT '数量',
  `category` varchar(32) DEFAULT NULL COMMENT '操作类型（入库、出库）',
  `client_name` varchar(32) DEFAULT NULL COMMENT '客户名称',
  `deliverer` varchar(255) DEFAULT NULL COMMENT '交货人',
  `operater_id` varchar(32) DEFAULT NULL COMMENT '操作人ID',
  `operater` varchar(32) DEFAULT NULL COMMENT '操作人',
  `delivery_address` varchar(32) DEFAULT NULL COMMENT '发货地址',
  `operate_time` varchar(255) DEFAULT NULL COMMENT '操作时间',
  `summary` varchar(32) DEFAULT NULL COMMENT '摘要',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
