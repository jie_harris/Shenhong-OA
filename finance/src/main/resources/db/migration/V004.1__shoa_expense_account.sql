/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : shoa-platform

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 24/01/2019 22:12:18
 明细账
*/

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `shoa_expense_account`;
CREATE TABLE `shoa_expense_account`  (
  `id` varchar(32) NOT NULL,
  `expense_date` varchar(255) DEFAULT NULL COMMENT '日期',
  `document_number` varchar(255) DEFAULT NULL COMMENT '凭证号',
  `coding` varchar(255) DEFAULT NULL COMMENT '科目编码',
  `accounting_subject` varchar(255) DEFAULT NULL COMMENT '会计科目(应收账款、应付账款、其他应收账款、其他应付账款)',
  `counterpart_subject` varchar(255) DEFAULT NULL COMMENT '对方科目',
  `summary` varchar(255) DEFAULT NULL COMMENT '摘要',
  `debit` varchar(255) DEFAULT NULL COMMENT '借方',
  `credit` varchar(255) DEFAULT NULL COMMENT '贷方',
  `transit_unit` varchar(255) DEFAULT NULL COMMENT '往来单位',
  `direction` varchar(255) DEFAULT NULL COMMENT '方向',
  `balance` varchar(255) DEFAULT NULL COMMENT '余额',
  `create_time` varchar(32) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
