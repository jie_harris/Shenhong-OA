/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : shoa-platform

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 24/01/2019 22:12:18
 库存管理-入库单
*/

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `shoa_goods`;
CREATE TABLE `shoa_goods`  (
  `id` varchar(32) NOT NULL,
  `goods_name` varchar(255) DEFAULT NULL COMMENT '货物名称',
  `material` varchar(255) DEFAULT NULL COMMENT '材质',
  `specification` varchar(255) DEFAULT NULL COMMENT '规格',
  `supplier` varchar(255) DEFAULT NULL COMMENT '供应商',
  `goods_type` varchar(32) DEFAULT NULL COMMENT '货物类型（原材料、半成品、成品）',
  `number` varchar(32) DEFAULT NULL COMMENT '数量',
  `price` varchar(32) DEFAULT NULL COMMENT '单价',
  `summary` varchar(255) DEFAULT NULL COMMENT '摘要',
  `flag` varchar(3) DEFAULT NULL COMMENT '删除标识(0未删除，-1删除)',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
