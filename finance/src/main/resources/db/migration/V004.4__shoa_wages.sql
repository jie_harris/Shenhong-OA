/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : shoa-platform

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 24/01/2019 22:12:18
 工资核算
*/

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `shoa_wages`;
CREATE TABLE `shoa_wages`  (
  `id` varchar(32) NOT NULL,
  `staff_name` varchar(255) DEFAULT NULL COMMENT '员工姓名',
  `fixed_salary` varchar(32) DEFAULT NULL COMMENT '固定工资',
  `user_id` varchar(32) DEFAULT NULL COMMENT '员工用户ID',
  `periods` varchar(32) DEFAULT NULL COMMENT '工资期数',
  `deduction_amount` varchar(32) DEFAULT NULL COMMENT '扣款金额',
  `deduction_desc` varchar(255) DEFAULT NULL COMMENT '扣款原因说明',
  `piece_amount` varchar(32) DEFAULT NULL COMMENT '计件金额',
  `piece_desc` varchar(255) DEFAULT NULL COMMENT '计件工资说明',
  `subsidy` varchar(32) DEFAULT NULL COMMENT '补贴',
  `subsidy_desc` varchar(255) DEFAULT NULL COMMENT '补贴说明',

  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
