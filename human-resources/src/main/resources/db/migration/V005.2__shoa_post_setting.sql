/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : shoa-platform

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 24/01/2019 22:12:18
 岗位设置
*/

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `shoa_post_setting`;
CREATE TABLE `shoa_post_setting`  (
  `id` varchar(32) NOT NULL,
  `post_name` varchar(255) DEFAULT NULL COMMENT '岗位名称',
  `post_welfare` varchar(255) DEFAULT NULL COMMENT '岗位福利',
  `post_function` TEXT DEFAULT NULL COMMENT '职能要求',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
