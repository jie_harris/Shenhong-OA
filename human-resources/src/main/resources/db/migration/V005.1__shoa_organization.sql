/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : shoa-platform

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 24/01/2019 22:12:18
 组织机构
*/

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `shoa_organization`;
CREATE TABLE `shoa_organization`  (
  `id` varchar(32) NOT NULL,
  `USCC` varchar(255) DEFAULT NULL COMMENT '统一社会信用代码',
  `OC` varchar(255) DEFAULT NULL COMMENT '组织机构代码',
  `register_name` varchar(255) DEFAULT NULL COMMENT '注册号',
  `nature` varchar(255) DEFAULT NULL COMMENT '公司性质',
  `company_name` varchar(255) DEFAULT NULL COMMENT '公司名称',
  `company_type` varchar(255) DEFAULT NULL COMMENT '公司类型',
  `establishment_date` varchar(32) DEFAULT NULL COMMENT '成立日期',
  `legal_representative` varchar(32) DEFAULT NULL COMMENT '法定代表人',
  `busniss_term` varchar(255) DEFAULT NULL COMMENT '营业期限',
  `opening_date` varchar(32) DEFAULT NULL COMMENT '发证日期',
  `registration_authority` varchar(255) DEFAULT NULL COMMENT '登记机关',
  `address` varchar(255) DEFAULT NULL COMMENT '企业地址',
  `business_scope` varchar(255) DEFAULT NULL COMMENT '经营范围',
  `company_profile` varchar(255) DEFAULT NULL COMMENT '公司简介',
  `duty` varchar(255) DEFAULT NULL COMMENT '税号',
  `phone` varchar(32) DEFAULT NULL COMMENT '电话号码',
  `deposit_bank` varchar(255) DEFAULT NULL COMMENT '开户银行',
  `bank_account` varchar(32) DEFAULT NULL COMMENT '银行账号',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

insert into `shoa_organization` values ('4b1b5af8401c11e98a921c872cb135b0','91510113327443073A','327443073','510113000079560','有限责任公司(自然人投资或控股)','成都申泓供水设备有限公司','有限责任公司(自然人投资或控股)','2015年01月20日','邓宁波','2015年01月20日--3999年01月01日','2016年03月28日','青白江区市场和质量监督管理局','成都市青白江区工业集中发展区创新路388号','研发、制造、销售：供水设备、净化设备、金属制品及配件；其他无需审批或许可的合法项目(以上依法须经批准的项目，经相关部门批准后方可开展经营活动）。','成都申泓供水设备有限公司于2015年01月20日成立。法定代表人邓宁波,公司经营范围包括：研发、制造、销售：供水设备、净化设备、金属制品及配件；其他无需审批或许可的合法项目(以上依法须经批准的项目，经相关部门批准后方可开展经营活动）等。','','','','');

SET FOREIGN_KEY_CHECKS = 1;
