/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : shoa-platform

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 24/01/2019 22:12:18
 薪酬管理
*/

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `shoa_payment`;
CREATE TABLE `shoa_payment`  (
  `id` varchar(32) NOT NULL,
  `staff_no` varchar(255) DEFAULT NULL COMMENT '员工编号',
  `staff_name` varchar(255) DEFAULT NULL COMMENT '员工姓名',
  `pay` varchar(255) DEFAULT NULL COMMENT '薪资',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
