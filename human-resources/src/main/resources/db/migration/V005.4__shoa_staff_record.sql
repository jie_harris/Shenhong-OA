/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : shoa-platform

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 24/01/2019 22:12:18
 员工档案
*/

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `shoa_staff_record`;
CREATE TABLE `shoa_staff_record`  (
  `id` varchar(32) NOT NULL,
  `name` varchar(32) DEFAULT NULL COMMENT '姓名',
  `gender` varchar(32) DEFAULT NULL COMMENT '性别',
  `nation` varchar(32) DEFAULT NULL COMMENT '民族',
  `birth_date` varchar(32) DEFAULT NULL COMMENT '出生日期',
  `identity_cerd_number` varchar(32) DEFAULT NULL COMMENT '身份证号码',
  `marital_status` varchar(255) DEFAULT NULL COMMENT '婚姻状况',
  `entry_time` varchar(32) DEFAULT NULL COMMENT '入职时间',
  `resignation_time` varchar(32) DEFAULT NULL COMMENT '离职时间',
  `native_place` varchar(32) DEFAULT NULL COMMENT '籍贯',
  `position` varchar(32) DEFAULT NULL COMMENT '所在职位',
  `department` varchar(32) DEFAULT NULL COMMENT '所在部门',
  `politics_status` varchar(32) DEFAULT NULL COMMENT '政治面貌',
  `highest_education` varchar(255) DEFAULT NULL COMMENT '最高学历',
  `williams_college` varchar(255) DEFAULT NULL COMMENT '毕业院校',
  `major` varchar(255) DEFAULT NULL COMMENT '专业',
  `phone` varchar(255) DEFAULT NULL COMMENT '联系电话',
  `qq` varchar(255) DEFAULT NULL COMMENT 'QQ号',
  `weichat` varchar(255) DEFAULT NULL COMMENT '微信号',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `present_address` varchar(255) DEFAULT NULL COMMENT '现住址',
  `domicile_place` varchar(255) DEFAULT NULL COMMENT '户口所在地',
  `flag` varchar(32) DEFAULT NULL COMMENT '是否离职（离职、未离职）',
  `labor_contract` TEXT DEFAULT NULL COMMENT '劳动合同图片',
  `signing_date` varchar(32) DEFAULT NULL COMMENT '劳动合同签订时间',
  `signing_time` varchar(32) DEFAULT NULL COMMENT '劳动合同签订时长',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
