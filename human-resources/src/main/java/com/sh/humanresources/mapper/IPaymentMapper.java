package com.sh.humanresources.mapper;

import com.sh.humanresources.base.BaseMapper;
import com.sh.humanresources.entity.Payment;
import org.springframework.stereotype.Component;

/**
 * @Author: he changjie on 2019/3/7
 * @Description:
 */
@Component
public interface IPaymentMapper extends BaseMapper<Payment> {
}
