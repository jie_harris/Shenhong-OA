package com.sh.humanresources.controller;

import com.google.common.collect.Maps;
import com.sh.humanresources.common.JsonMapper;
import com.sh.humanresources.common.ResultUtil;
import com.sh.humanresources.entity.Organization;
import com.sh.humanresources.entity.Payment;
import com.sh.humanresources.entity.PostSet;
import com.sh.humanresources.entity.StaffRecord;
import com.sh.humanresources.enums.ResultEnum;
import com.sh.humanresources.handle.ExceptionHandle;
import com.sh.humanresources.service.OrganizationService;
import com.sh.humanresources.service.PaymentService;
import com.sh.humanresources.service.PostSetService;
import com.sh.humanresources.service.StaffRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: he changjie on 2019/3/7
 * @Description:
 */
@RestController
@RequestMapping("/hr")
public class HRController {
    private JsonMapper jsonMapper = JsonMapper.INSTANCE;
    @Autowired
    private ExceptionHandle exceptionHandle;
    @Autowired
    private StaffRecordService staffRecordService;
    @Autowired
    private PostSetService postSetService;
    @Autowired
    private PaymentService paymentService;
    @Autowired
    private OrganizationService organizationService;

    /**
     * 新增一个薪酬
     * @param payment 薪酬实体
     * @return ResponseEntity
     */
    @RequestMapping(value = "/payment/add",method = RequestMethod.POST)
    public ResponseEntity addPayment(@RequestBody Payment payment) {
        try {
            int add = paymentService.add(payment);
            if(add!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 新增一个岗位
     * @param postSet 岗位实体
     * @return ResponseEntity
     */
    @RequestMapping(value = "/postset/add",method = RequestMethod.POST)
    public ResponseEntity addPostSet(@RequestBody PostSet postSet) {
        try {
            int add = postSetService.add(postSet);
            if(add!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 新增一个员工
     * @param staffRecord 员工实体
     * @return ResponseEntity
     */
    @RequestMapping(value = "/staff/add",method = RequestMethod.POST)
    public ResponseEntity addStaff(@RequestBody StaffRecord staffRecord) {
        try {
            int add = staffRecordService.add(staffRecord);
            if(add!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 删除一个薪酬
     * @param id 薪酬实体
     * @return ResponseEntity
     */
    @RequestMapping(value = "/payment/delete/{id}",method = RequestMethod.GET)
    public ResponseEntity deletePayment(@PathVariable String id) {
        try {
            int delete = paymentService.delete(id);
            if(delete!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 删除一个岗位
     * @param id 岗位实体
     * @return ResponseEntity
     */
    @RequestMapping(value = "/postset/delete/{id}",method = RequestMethod.GET)
    public ResponseEntity deletePostSet(@PathVariable String id) {
        try {
            int delete = postSetService.delete(id);
            if(delete!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 删除一个员工
     * @param id 员工实体
     * @return ResponseEntity
     */
    @RequestMapping(value = "/staff/delete/{id}",method = RequestMethod.GET)
    public ResponseEntity deleteStaff(@PathVariable String id) {
        try {
            int delete = staffRecordService.delete(id);
            if(delete!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 修改一个薪酬
     * @param payment 薪酬实体
     * @return ResponseEntity
     */
    @RequestMapping(value = "/payment/update",method = RequestMethod.POST)
    public ResponseEntity updatePayment(@RequestBody Payment payment) {
        try {
            int update = paymentService.update(payment);
            if(update!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 修改一个岗位
     * @param postSet 岗位实体
     * @return ResponseEntity
     */
    @RequestMapping(value = "/postset/update",method = RequestMethod.POST)
    public ResponseEntity updatePostSet(@RequestBody PostSet postSet) {
        try {
            int update = postSetService.update(postSet);
            if(update!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 修改一个员工
     * @param staffRecord 员工实体
     * @return ResponseEntity
     */
    @RequestMapping(value = "/staff/update",method = RequestMethod.POST)
    public ResponseEntity updateStaff(@RequestBody StaffRecord staffRecord) {
        try {
            int update = staffRecordService.update(staffRecord);
            if(update!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 更新组织机构的信息
     * @param organization 组织机构实体
     * @return ResponseEntity
     */
    @RequestMapping(value = "/organ/update",method = RequestMethod.POST)
    public ResponseEntity updateOrgan(@RequestBody Organization organization) {
        try {
            int update = organizationService.update(organization);
            if(update!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 分页
     * 查询符合搜索条件的薪酬
     * @return
     */
    @RequestMapping(value = "/payment/queryAll/{params}",method = RequestMethod.GET)
    public ResponseEntity queryAllPayment(@PathVariable String params){
        try {
            Map fromJson = jsonMapper.fromJson(params, Map.class);
            String search = (String)fromJson.get("search");
            int currentPage = (int)fromJson.get("currentPage");
            int pageSize = (int)fromJson.get("pageSize");
            List<Payment> list = paymentService.queryAll(search,currentPage,pageSize);
            if(list!=null){
                HashMap<String, Object> map = Maps.newHashMap();
                map.put("list",list);
                map.put("total",paymentService.queryAll(search));
                map.put("currentPage",currentPage);
                map.put("pageSize",pageSize);
                return new ResponseEntity(ResultUtil.success(map), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 分页
     * 查询符合搜索条件的岗位
     * @return
     */
    @RequestMapping(value = "/postset/queryAll/{params}",method = RequestMethod.GET)
    public ResponseEntity queryAllPostSet(@PathVariable String params){
        try {
            Map fromJson = jsonMapper.fromJson(params, Map.class);
            String search = (String)fromJson.get("search");
            int currentPage = (int)fromJson.get("currentPage");
            int pageSize = (int)fromJson.get("pageSize");
            List<PostSet> list = postSetService.queryAll(search,currentPage,pageSize);
            if(list!=null){
                HashMap<String, Object> map = Maps.newHashMap();
                map.put("list",list);
                map.put("total",postSetService.queryAll(search));
                map.put("currentPage",currentPage);
                map.put("pageSize",pageSize);
                return new ResponseEntity(ResultUtil.success(map), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 查询所有在职的员工
     * @return
     */
    @RequestMapping(value = "/staff/queryAllW",method = RequestMethod.GET)
    public ResponseEntity queryAllStaff(){
        try {
            List<StaffRecord> list = staffRecordService.queryAll();
            if(list!=null){
                return new ResponseEntity(ResultUtil.success(list), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 分页
     * 查询符合搜索条件的员工
     * @return
     */
    @RequestMapping(value = "/staff/queryAll/{params}",method = RequestMethod.GET)
    public ResponseEntity queryAllStaff(@PathVariable String params){
        try {
            Map fromJson = jsonMapper.fromJson(params, Map.class);
            String search = (String)fromJson.get("search");
            int currentPage = (int)fromJson.get("currentPage");
            int pageSize = (int)fromJson.get("pageSize");
            List<StaffRecord> list = staffRecordService.queryAll(search,currentPage,pageSize);
            if(list!=null){
                HashMap<String, Object> map = Maps.newHashMap();
                map.put("list",list);
                map.put("total",staffRecordService.queryAll(search));
                map.put("currentPage",currentPage);
                map.put("pageSize",pageSize);
                return new ResponseEntity(ResultUtil.success(map), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 通过ID查询薪酬信息
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/payment/queryOne/{id}",method = RequestMethod.GET)
    public ResponseEntity queryOnePayment(@PathVariable String id){
        try {
            Payment payment = paymentService.queryOne(id);
            if(payment!=null){
                return new ResponseEntity(ResultUtil.success(payment), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/payment/queryOneByNo/{id}",method = RequestMethod.GET)
    public ResponseEntity queryOnePaymentByStaffNo(@PathVariable String id){
        try {
            Payment payment = paymentService.queryOneByStaffNo(id);
            if(payment!=null){
                return new ResponseEntity(ResultUtil.success(payment), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 通过ID查询岗位信息
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/postset/queryOne/{id}",method = RequestMethod.GET)
    public ResponseEntity queryOnePostSet(@PathVariable String id){
        try {
            PostSet postSet = postSetService.queryOne(id);
            if(postSet!=null){
                return new ResponseEntity(ResultUtil.success(postSet), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 通过ID查询员工信息
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/staff/queryOne/{id}",method = RequestMethod.GET)
    public ResponseEntity queryOneStaff(@PathVariable String id){
        try {
            StaffRecord staffRecord = staffRecordService.queryOne(id);
            if(staffRecord!=null){
                return new ResponseEntity(ResultUtil.success(staffRecord), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 查询组织机构
     * @return
     */
    @RequestMapping(value = "/organ/query",method = RequestMethod.GET)
    public ResponseEntity queryOrgan(){
        try {
            Organization organization = organizationService.query();
            if(organization!=null){
                return new ResponseEntity(ResultUtil.success(organization), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

}
