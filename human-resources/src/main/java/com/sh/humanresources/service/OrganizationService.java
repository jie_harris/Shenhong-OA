package com.sh.humanresources.service;

import com.sh.humanresources.entity.Organization;
import com.sh.humanresources.mapper.IOrganizationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: he changjie on 2019/3/7
 * @Description:
 */
@Service
public class OrganizationService {
    @Autowired
    private IOrganizationMapper organizationMapper;
    /**
     * 更新组织机构的信息
     * @param organization 组织机构实体
     * @return 1：成功
     */
    public int update(Organization organization){
        return organizationMapper.updateByPrimaryKeySelective(organization);
    }

    /**
     * 查询组织机构
     * @return PostSet
     */
    public Organization query(){
        List<Organization> organizations = organizationMapper.selectAll();
        return organizations!=null && organizations.size()==1?organizations.get(0):null;
    }
}
