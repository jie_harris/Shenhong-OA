package com.sh.humanresources.service;

import com.sh.humanresources.entity.StaffRecord;
import com.sh.humanresources.mapper.IStaffRecordMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import java.util.List;

/**
 * @Author: he changjie on 2019/3/7
 * @Description:
 */
@Service
public class StaffRecordService {
    @Autowired
    private IStaffRecordMapper staffRecordMapper;


    /**
     * 新增一个员工
     * @param staffRecord 员工实体
     * @return 1：成功
     */
    public int add(StaffRecord staffRecord){
        return staffRecordMapper.insertSelective(staffRecord);
    }

    /**
     * 删除一个员工
     * @param id 员工ID
     * @return 1：成功
     */
    public int delete(String id){
        return staffRecordMapper.deleteByPrimaryKey(id);
    }

    /**
     * 更新一个员工信息
     * @param staffRecord 员工实体
     * @return 1：成功
     */
    public int update(StaffRecord staffRecord){
        return staffRecordMapper.updateByPrimaryKeySelective(staffRecord);
    }

    /**
     * 通过ID查询员工信息
     * @param id ID
     * @return StaffRecord
     */
    public StaffRecord queryOne(String id){
        return staffRecordMapper.selectByPrimaryKey(id);
    }

    /**
     * 查询所有在职的员工
     * @return List<StaffRecord>
     */
    public List<StaffRecord> queryAll(){
        Example example=new Example(StaffRecord.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("flag","未离职");
        return staffRecordMapper.selectByExample(example);
    }

    /**
     * 分页
     * 查询符合搜索条件的员工
     * @param search 员工姓名
     * @param currentPage 当前页号
     * @param pageSize 页尺寸
     * @return List<StaffRecord>
     */
    public List<StaffRecord> queryAll(String search, int currentPage, int pageSize){
        RowBounds rowBunds = new RowBounds((currentPage - 1) * pageSize, pageSize);
        Example example=new Example(StaffRecord.class);
        Example.Criteria criteria = example.createCriteria();
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("name","%"+search+"%");
        }
        return staffRecordMapper.selectByExampleAndRowBounds(example,rowBunds);
    }

    /**
     * 分页
     * 查询记录数
     * @param search 员工姓名
     * @return 记录数
     */
    public int queryAll(String search){
        Example example=new Example(StaffRecord.class);
        Example.Criteria criteria = example.createCriteria();
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("name","%"+search+"%");
        }
        return staffRecordMapper.selectCountByExample(example);
    }

}
