package com.sh.humanresources.service;

import com.sh.humanresources.entity.PostSet;
import com.sh.humanresources.mapper.IPostSetMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.plugin.javascript.navig.Link;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @Author: he changjie on 2019/3/7
 * @Description:
 */
@Service
public class PostSetService {
    @Autowired
    private IPostSetMapper postSetMapper;

    /**
     * 新增一个岗位
     * @param postSet 岗位实体
     * @return 1：成功
     */
    public int add(PostSet postSet){
        return postSetMapper.insertSelective(postSet);
    }

    /**
     * 删除一个岗位
     * @param id 岗位ID
     * @return 1：成功
     */
    public int delete(String id){
        return postSetMapper.deleteByPrimaryKey(id);
    }

    /**
     * 更新一个岗位信息
     * @param postSet 岗位实体
     * @return 1：成功
     */
    public int update(PostSet postSet){
        return postSetMapper.updateByPrimaryKeySelective(postSet);
    }

    /**
     * 通过ID查询岗位信息
     * @param id ID
     * @return PostSet
     */
    public PostSet queryOne(String id){
        return postSetMapper.selectByPrimaryKey(id);
    }

    /**
     * 分页
     * 查询符合搜索条件的岗位
     * @param search 岗位名称
     * @param currentPage 当前页号
     * @param pageSize 页尺寸
     * @return List<PostSet>
     */
    public List<PostSet> queryAll(String search,int currentPage,int pageSize){
        RowBounds rowBunds = new RowBounds((currentPage - 1) * pageSize, pageSize);
        Example example=new Example(PostSet.class);
        Example.Criteria criteria = example.createCriteria();
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("postName","%"+search+"%");
        }
        return postSetMapper.selectByExampleAndRowBounds(example,rowBunds);
    }

    /**
     * 分页
     * 查询记录数
     * @param search 岗位名称
     * @return 记录数
     */
    public int queryAll(String search){
        Example example=new Example(PostSet.class);
        Example.Criteria criteria = example.createCriteria();
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("postName","%"+search+"%");
        }
        return postSetMapper.selectCountByExample(example);
    }
}
