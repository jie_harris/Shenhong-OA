package com.sh.humanresources.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sh.humanresources.entity.Payment;
import com.sh.humanresources.entity.StaffRecord;
import com.sh.humanresources.mapper.IPaymentMapper;
import com.sh.humanresources.mapper.IStaffRecordMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Author: he changjie on 2019/3/7
 * @Description:
 */
@Service
public class PaymentService {
    @Autowired
    private IPaymentMapper paymentMapper;
    @Autowired
    private IStaffRecordMapper staffRecordMapper;

    /**
     * 新增一个薪酬
     * @param payment 薪酬实体
     * @return 1：成功
     */
    public int add(Payment payment){
        return paymentMapper.insertSelective(payment);
    }

    /**
     * 删除一个薪酬
     * @param id 薪酬ID
     * @return 1：成功
     */
    public int delete(String id){
        return paymentMapper.deleteByPrimaryKey(id);
    }

    /**
     * 更新一个薪酬信息
     * @param payment 薪酬实体
     * @return 1：成功
     */
    public int update(Payment payment){
        return paymentMapper.updateByPrimaryKeySelective(payment);
    }

    /**
     * 通过ID查询薪酬信息
     * @param id ID
     * @return Payment
     */
    public Payment queryOne(String id){
        return paymentMapper.selectByPrimaryKey(id);
    }

    /**
     * 通过员工ID查询员工薪资
     * @param id ID
     * @return Payment
     */
    public Payment queryOneByStaffNo(String id){
        Example example = new Example(Payment.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("staffNo",id);
        List<Payment> payments = paymentMapper.selectByExample(example);
        return payments!=null&&payments.size()>0?payments.get(0):null;
    }

    /**
     * 分页
     * 查询符合搜索条件的薪酬
     * @param search 员工姓名
     * @param currentPage 当前页号
     * @param pageSize 页尺寸
     * @return List<PostSet>
     */
    public List<Payment> queryAll(String search, int currentPage, int pageSize){
        RowBounds rowBunds = new RowBounds((currentPage - 1) * pageSize, pageSize);
        Example example=new Example(Payment.class);
        Example.Criteria criteria = example.createCriteria();
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("staffName","%"+search+"%");
        }
        return paymentMapper.selectByExampleAndRowBounds(example,rowBunds);
    }

    /**
     * 分页
     * 查询记录数
     * @param search 员工姓名
     * @return 记录数
     */
    public int queryAll(String search){
        Example example=new Example(Payment.class);
        Example.Criteria criteria = example.createCriteria();
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("staffName","%"+search+"%");
        }
        return paymentMapper.selectCountByExample(example);
    }
}
