package com.sh.humanresources.entity;


import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "shoa_payment")
public class Payment {

  @Id
  @GeneratedValue(generator = "UUID")
  private String id;
  private String staffNo;
  private String staffName;
  private String pay;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getStaffNo() {
    return staffNo;
  }

  public void setStaffNo(String staffNo) {
    this.staffNo = staffNo;
  }


  public String getStaffName() {
    return staffName;
  }

  public void setStaffName(String staffName) {
    this.staffName = staffName;
  }


  public String getPay() {
    return pay;
  }

  public void setPay(String pay) {
    this.pay = pay;
  }

}
