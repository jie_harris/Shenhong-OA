package com.sh.humanresources.entity;


import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "shoa_post_setting")
public class PostSet {

  @Id
  @GeneratedValue(generator = "UUID")
  private String id;
  private String postName;
  private String postWelfare;
  private String postFunction;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getPostName() {
    return postName;
  }

  public void setPostName(String postName) {
    this.postName = postName;
  }


  public String getPostWelfare() {
    return postWelfare;
  }

  public void setPostWelfare(String postWelfare) {
    this.postWelfare = postWelfare;
  }


  public String getPostFunction() {
    return postFunction;
  }

  public void setPostFunction(String postFunction) {
    this.postFunction = postFunction;
  }

}
