package com.sh.humanresources.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "shoa_staff_record")
public class StaffRecord {

  @Id
  @GeneratedValue(generator = "UUID")
  private String id;
  private String name;
  private String gender;
  private String nation;
  private String birthDate;
  private String identityCerdNumber;
  private String maritalStatus;
  private String entryTime;
  private String resignationTime;
  private String nativePlace;
  private String position;
  private String department;
  private String politicsStatus;
  private String highestEducation;
  private String williamsCollege;
  private String major;
  private String phone;
  private String qq;
  private String weichat;
  private String email;
  private String presentAddress;
  private String domicilePlace;
  private String flag;
  private String laborContract;
  private String signingDate;
  private String signingTime;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }


  public String getNation() {
    return nation;
  }

  public void setNation(String nation) {
    this.nation = nation;
  }


  public String getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(String birthDate) {
    this.birthDate = birthDate;
  }


  public String getIdentityCerdNumber() {
    return identityCerdNumber;
  }

  public void setIdentityCerdNumber(String identityCerdNumber) {
    this.identityCerdNumber = identityCerdNumber;
  }


  public String getMaritalStatus() {
    return maritalStatus;
  }

  public void setMaritalStatus(String maritalStatus) {
    this.maritalStatus = maritalStatus;
  }


  public String getEntryTime() {
    return entryTime;
  }

  public void setEntryTime(String entryTime) {
    this.entryTime = entryTime;
  }


  public String getResignationTime() {
    return resignationTime;
  }

  public void setResignationTime(String resignationTime) {
    this.resignationTime = resignationTime;
  }


  public String getNativePlace() {
    return nativePlace;
  }

  public void setNativePlace(String nativePlace) {
    this.nativePlace = nativePlace;
  }


  public String getPosition() {
    return position;
  }

  public void setPosition(String position) {
    this.position = position;
  }


  public String getDepartment() {
    return department;
  }

  public void setDepartment(String department) {
    this.department = department;
  }


  public String getPoliticsStatus() {
    return politicsStatus;
  }

  public void setPoliticsStatus(String politicsStatus) {
    this.politicsStatus = politicsStatus;
  }


  public String getHighestEducation() {
    return highestEducation;
  }

  public void setHighestEducation(String highestEducation) {
    this.highestEducation = highestEducation;
  }


  public String getWilliamsCollege() {
    return williamsCollege;
  }

  public void setWilliamsCollege(String williamsCollege) {
    this.williamsCollege = williamsCollege;
  }


  public String getMajor() {
    return major;
  }

  public void setMajor(String major) {
    this.major = major;
  }


  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }


  public String getQq() {
    return qq;
  }

  public void setQq(String qq) {
    this.qq = qq;
  }


  public String getWeichat() {
    return weichat;
  }

  public void setWeichat(String weichat) {
    this.weichat = weichat;
  }


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  public String getPresentAddress() {
    return presentAddress;
  }

  public void setPresentAddress(String presentAddress) {
    this.presentAddress = presentAddress;
  }


  public String getDomicilePlace() {
    return domicilePlace;
  }

  public void setDomicilePlace(String domicilePlace) {
    this.domicilePlace = domicilePlace;
  }


  public String getFlag() {
    return flag;
  }

  public void setFlag(String flag) {
    this.flag = flag;
  }


  public String getLaborContract() {
    return laborContract;
  }

  public void setLaborContract(String laborContract) {
    this.laborContract = laborContract;
  }


  public String getSigningDate() {
    return signingDate;
  }

  public void setSigningDate(String signingDate) {
    this.signingDate = signingDate;
  }


  public String getSigningTime() {
    return signingTime;
  }

  public void setSigningTime(String signingTime) {
    this.signingTime = signingTime;
  }

}
