package com.sh.humanresources.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "shoa_organization")
public class Organization {

  @Id
  @GeneratedValue(generator = "UUID")
  private String id;
  private String uscc;
  private String oc;
  private String registerName;
  private String nature;
  private String companyName;
  private String companyType;
  private String establishmentDate;
  private String legalRepresentative;
  private String busnissTerm;
  private String openingDate;
  private String registrationAuthority;
  private String address;
  private String businessScope;
  private String companyProfile;
  private String duty;
  private String phone;
  private String depositBank;
  private String bankAccount;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getUscc() {
    return uscc;
  }

  public void setUscc(String uscc) {
    this.uscc = uscc;
  }


  public String getOc() {
    return oc;
  }

  public void setOc(String oc) {
    this.oc = oc;
  }


  public String getRegisterName() {
    return registerName;
  }

  public void setRegisterName(String registerName) {
    this.registerName = registerName;
  }


  public String getNature() {
    return nature;
  }

  public void setNature(String nature) {
    this.nature = nature;
  }


  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }


  public String getCompanyType() {
    return companyType;
  }

  public void setCompanyType(String companyType) {
    this.companyType = companyType;
  }


  public String getEstablishmentDate() {
    return establishmentDate;
  }

  public void setEstablishmentDate(String establishmentDate) {
    this.establishmentDate = establishmentDate;
  }


  public String getLegalRepresentative() {
    return legalRepresentative;
  }

  public void setLegalRepresentative(String legalRepresentative) {
    this.legalRepresentative = legalRepresentative;
  }


  public String getBusnissTerm() {
    return busnissTerm;
  }

  public void setBusnissTerm(String busnissTerm) {
    this.busnissTerm = busnissTerm;
  }


  public String getOpeningDate() {
    return openingDate;
  }

  public void setOpeningDate(String openingDate) {
    this.openingDate = openingDate;
  }


  public String getRegistrationAuthority() {
    return registrationAuthority;
  }

  public void setRegistrationAuthority(String registrationAuthority) {
    this.registrationAuthority = registrationAuthority;
  }


  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }


  public String getBusinessScope() {
    return businessScope;
  }

  public void setBusinessScope(String businessScope) {
    this.businessScope = businessScope;
  }


  public String getCompanyProfile() {
    return companyProfile;
  }

  public void setCompanyProfile(String companyProfile) {
    this.companyProfile = companyProfile;
  }


  public String getDuty() {
    return duty;
  }

  public void setDuty(String duty) {
    this.duty = duty;
  }


  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }


  public String getDepositBank() {
    return depositBank;
  }

  public void setDepositBank(String depositBank) {
    this.depositBank = depositBank;
  }


  public String getBankAccount() {
    return bankAccount;
  }

  public void setBankAccount(String bankAccount) {
    this.bankAccount = bankAccount;
  }

}
