package com.sh.gateway.service;

import com.sh.gateway.entity.PermissionMenu;
import com.sh.gateway.entity.Role;
import com.sh.gateway.mapper.IPermissionMenuMapper;
import com.sh.gateway.mapper.IRoleMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Author: he changjie on 2019/2/10
 * @Description:
 */
@Service
public class PermissionMenuServcie {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private IPermissionMenuMapper menuMapper;
    @Autowired
    private IRoleMapper roleMapper;

    /**
     * 添加一项权限菜单
     * @param menu 权限菜单
     * @return
     */
    public int add(PermissionMenu menu){
        return menuMapper.insertSelective(menu);
    }

    public String queryIdByName(String name){
        if(StringUtils.isNotEmpty(name)){
            Example example=new Example(PermissionMenu.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("menuName",name);
            List<PermissionMenu> menus = menuMapper.selectByExample(example);
            return menus!=null&&menus.size()>0?menus.get(0).getId():null;
        }
        return null;
    }

    /**
     * 修改一项权限菜单
     * @param menu 权限菜单
     * @return
     */
    public int update(PermissionMenu menu){
        return menuMapper.updateByPrimaryKeySelective(menu);
    }

    /**
     * 删除一项权限菜单
     * 级联将含有该项权限的角色移除该项权限
     * @param id 权限菜单
     * @return
     */
    public int delete(String id){
        Example example = new Example(Role.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("menuId",id);
        List<Role> roleList = roleMapper.selectByExample(example);
        if(roleList!=null && roleList.size()>0){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String deleteTime = dateFormat.format(new Date());
            for (Role role : roleList) {
                role.setFlag("-1");
                role.setDeleteTime(deleteTime);
                roleMapper.updateByPrimaryKeySelective(role);
            }
        }
        return menuMapper.deleteByPrimaryKey(id);
    }

    /**
     * 分页
     * 拉取所有权限菜单
     * @return
     */
    public List<PermissionMenu> queryAll(String search,int currentPage,int pageSize){
        RowBounds rowBunds = new RowBounds((currentPage - 1) * pageSize, pageSize);
        Example example=new Example(PermissionMenu.class);
        Example.Criteria criteria = example.createCriteria();
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("menuName","%"+search+"%");
        }
        return menuMapper.selectByExampleAndRowBounds(example,rowBunds);
    }

    /**
     * 通过主键查询权限菜单
     * @param id ID
     * @return
     */
    public PermissionMenu queryOne(String id){
        return menuMapper.selectByPrimaryKey(id);
    }

    /**
     * 分页
     * 拉取所有权限菜单的数量
     * @return
     */
    public int queryAll(String search){
        Example example=new Example(PermissionMenu.class);
        Example.Criteria criteria = example.createCriteria();
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("menuName","%"+search+"%");
        }
        return menuMapper.selectCountByExample(example);
    }

    /**
     * 根据权限等级拉取权限菜单
     * @param level 权限等级
     * @return
     */
    public List<PermissionMenu> queryAllByLevel(String level){
        Example example = new Example(PermissionMenu.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("menuLevel",level);
        return menuMapper.selectByExample(example);
    }
}
