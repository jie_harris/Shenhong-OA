package com.sh.gateway.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sh.gateway.entity.User;
import com.sh.gateway.mapper.IUserMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Author: he changjie on 2019/2/10
 * @Description:
 */
@Service
public class UserService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private IUserMapper userMapper;

    /**
     * 用户登录，成功则返回该用户实体，失败则返回null
     * @param user 用户
     * @return
     */
    public User login(User user){
        if(StringUtils.isEmpty(user.getUsername())||StringUtils.isEmpty(user.getPassword())){
            return null;
        }
        Example example = new Example(User.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("username",user.getUsername());
        List<User> users = userMapper.selectByExample(example);
        if(users!=null && users.size()==1){
            User user1 = users.get(0);
            if (user1.getPassword().equals(user.getPassword())) {
                return user1;
            }
        }
        return null;
    }

    /**
     * 添加用户
     * 用户登录名唯一
     * @param user 用户
     * @return
     */
    public int add(User user){
        Example example = new Example(User.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("username",user.getUsername());
        List<User> users = userMapper.selectByExample(example);
        if(users!=null&&users.size()>0){
            return -2;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String createTime = dateFormat.format(new Date());
        user.setCreateTime(createTime);

        return userMapper.insertSelective(user);
    }

    /**
     * 赋予用户某一角色
     * @param id 用户ID
     * @param roleId 角色ID
     * @return
     */
    public int give(String id,String roleId){
        if(StringUtils.isEmpty(id) || StringUtils.isEmpty(roleId)){
            return -1;
        }
        User user = userMapper.selectByPrimaryKey(id);
        user.setRoleId(roleId);
        return userMapper.updateByPrimaryKeySelective(user);
    }

    /**
     * 更新用户信息
     * @param user 用户
     * @return
     */
    public int update(User user){
        return userMapper.updateByPrimaryKeySelective(user);
    }

    /**
     * 用户修改密码
     * @param id 用户ID
     * @param pwd 新密码
     * @param opwd 原密码
     * @return
     */
    public int updatePWD(String id,String pwd,String opwd){
        User user = queryOne(id);
        if(opwd.equals(user.getPassword())){
            user.setPassword(pwd);
            return update(user);
        }else{
            return -1;
        }
    }

    /**
     * 通过ID查询用户
     * @param id ID
     * @return
     */
    public User queryOne(String id){
        return userMapper.selectByPrimaryKey(id);
    }

    /**
     * 返回除了自己的所有用户的ID和昵称
     * @return List<Object>
     */
    public List<Object> queryS(String id){
        ArrayList<Object> list = Lists.newArrayList();
        HashMap<Object, Object> map;
        List<User> users = userMapper.selectAll();
        if(users!=null && users.size()>0){
            for (User user : users) {
                map = Maps.newHashMap();
                if(!id.equals(user.getId())){
                    map.put("id",user.getId());
                    map.put("nickname",user.getNickname());
                    list.add(map);
                }
            }
        }
        return list;
    }

    /**
     * 删除用户
     * @param id 用户ID
     * @return
     */
    public int delete(String id){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String deleteTime = dateFormat.format(new Date());
        User user = userMapper.selectByPrimaryKey(id);
        user.setFlag("-1");
        user.setDeleteTime(deleteTime);
        return userMapper.updateByPrimaryKeySelective(user);
    }

    /**
     * 分页
     * 拉取所有用户信息列表
     * @return
     */
    public List<User> queryAll(String search,int currentPage,int pageSize){
        RowBounds rowBunds = new RowBounds((currentPage - 1) * pageSize, pageSize);
        Example example=new Example(User.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andIsNull("flag");
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("nickname","%"+search+"%");
        }
        return userMapper.selectByExampleAndRowBounds(example,rowBunds);
    }
    /**
     * 拉取用户总数量
     * @return
     */
    public int queryAll(String search){
        Example example=new Example(User.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andIsNull("flag");
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("nickname","%"+search+"%");
        }
        return userMapper.selectCountByExample(example);
    }
}
