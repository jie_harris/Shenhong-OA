package com.sh.gateway.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author he changjie
 */
@Table(name = "shoa_permission_menu")
public class PermissionMenu implements Serializable {
  @Id
  @GeneratedValue(generator = "UUID")
  private String id;
  private String icon;
  private String path;
  private String name;
  private String menuLevel;
  private String menuName;
  private String menuDesc;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }


  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getMenuLevel() {
    return menuLevel;
  }

  public void setMenuLevel(String menuLevel) {
    this.menuLevel = menuLevel;
  }


  public String getMenuName() {
    return menuName;
  }

  public void setMenuName(String menuName) {
    this.menuName = menuName;
  }


  public String getMenuDesc() {
    return menuDesc;
  }

  public void setMenuDesc(String menuDesc) {
    this.menuDesc = menuDesc;
  }

}
