package com.sh.gateway.mapper;

import com.sh.gateway.base.BaseMapper;
import com.sh.gateway.entity.User;
import org.springframework.stereotype.Component;

/**
 * @Author: he changjie on 2019/2/10
 * @Description:
 */
@Component
public interface IUserMapper extends BaseMapper<User> {
}
