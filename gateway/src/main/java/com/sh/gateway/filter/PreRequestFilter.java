package com.sh.gateway.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: he changjie on 2019/2/10
 * @Description:
 */
public class PreRequestFilter extends ZuulFilter {
    private final static Logger logger = LoggerFactory.getLogger(PreRequestFilter.class);

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return false;
    }

    @Override
    public Object run() {
        RequestContext ct=RequestContext.getCurrentContext();
        HttpServletRequest request = ct.getRequest();

        return null;
    }
}
