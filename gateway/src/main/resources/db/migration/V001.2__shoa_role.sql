/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : shoa-platform

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 24/01/2019 22:12:26
*/

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `shoa_role`;
CREATE TABLE `shoa_role`  (
  `role_id` varchar(32)  DEFAULT NULL COMMENT '角色ID，可重复',
  `role_name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `menu_id` varchar(255) DEFAULT NULL COMMENT '权限菜单ID',
  `create_time` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `flag` varchar(3) DEFAULT NULL COMMENT '是否删除标记',
  `delete_time` varchar(32) DEFAULT NULL COMMENT '删除时间'
)ENGINE = InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `shoa_role` VALUES ( '1550131242206', '超级管理员', 'b8b82a921fdf11e989691c872cb135b0', '2019-01-24 22:06', NULL, NULL);
INSERT INTO `shoa_role` VALUES ( '1550131242206', '超级管理员', '44a11dd61fe311e989691c872cb135b0', '2019-01-24 22:06', NULL, NULL);
INSERT INTO `shoa_role` VALUES ( '1550131242206', '超级管理员', 'd8c5fac81fe311e989691c872cb135b0', '2019-01-24 22:06', NULL, NULL);
INSERT INTO `shoa_role` VALUES ( '1550131242206', '超级管理员', 'e20695c41fe311e989691c872cb135b0', '2019-01-24 22:06', NULL, NULL);
INSERT INTO `shoa_role` VALUES ( '1550131242206', '超级管理员', 'f37ba8831fe311e989691c872cb135b0', '2019-01-24 22:06', NULL, NULL);
INSERT INTO `shoa_role` VALUES ( '1550131242206', '超级管理员', '18d1be211fe411e989691c872cb135b0', '2019-01-24 22:06', NULL, NULL);
INSERT INTO `shoa_role` VALUES ( '1550131242206', '超级管理员', '4ed6d1672cd211e98d6d1c872cb135b0', '2019-01-24 22:06', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
