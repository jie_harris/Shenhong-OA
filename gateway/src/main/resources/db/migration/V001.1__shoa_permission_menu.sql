/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : shoa-platform

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 24/01/2019 22:12:18
*/

SET FOREIGN_KEY_CHECKS = 0;


DROP TABLE IF EXISTS `shoa_permission_menu`;
CREATE TABLE `shoa_permission_menu`  (
  `id` varchar(32) NOT NULL,
  `icon` varchar(255) DEFAULT NULL COMMENT '图标路径',
  `path` varchar(255) DEFAULT NULL UNIQUE COMMENT 'router路由路径',
  `name` varchar(255) DEFAULT NULL UNIQUE COMMENT 'router路由名称',
  `menu_level` varchar(4) DEFAULT NULL COMMENT 'router路由等级',
  `menu_name` varchar(255) DEFAULT NULL UNIQUE COMMENT '名称',
  `menu_desc` varchar(255) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `shoa_permission_menu` VALUES ('18d1be211fe411e989691c872cb135b0', 'home_content_sr.png', '/finance','finance', '1', '财务管理', '报表管理、资金、工作核算、库存、项目、资料管理');
INSERT INTO `shoa_permission_menu` VALUES ('b8b82a921fdf11e989691c872cb135b0', 'home_content_jqzx.png', '/ac', 'ac','1', '鉴权中心', '权限菜单管理、角色管理、用户管理、权限收发');
INSERT INTO `shoa_permission_menu` VALUES ('44a11dd61fe311e989691c872cb135b0', 'home_content_rlzy.png', '/hr', 'hr', '1', '人力资源管理', '人事日常事务薪酬、招聘、培训、考核以及人力资源的管理');
INSERT INTO `shoa_permission_menu` VALUES ('d8c5fac81fe311e989691c872cb135b0', 'home_content_tgxx.png', '/notice', 'notice', '1', '通告信息管理', '通告创建、审核通告、通告下发、通告追溯、通告查询');
INSERT INTO `shoa_permission_menu` VALUES ('e20695c41fe311e989691c872cb135b0', 'home_content_gzjh.png', '/wp', 'wp', '1', '工作计划管理', '创建任务、编辑任务、评审任务、执行任务、关闭任务');
INSERT INTO `shoa_permission_menu` VALUES ('f37ba8831fe311e989691c872cb135b0', 'home_content_wjxt.png', '/fs', 'fs', '1', '文件系统管理', '上传文件、下载文件、共享文件、文件管理、文件列表');
-- INSERT INTO `shoa_permission_menu` VALUES ('4ed6d1672cd211e98d6d1c872cb135b0', 'home_content_gwxx.png', '/od', 'od', '1', '公文信息管理', '收发公文、公文预览、公文修改、公文归档、公文追溯');

SET FOREIGN_KEY_CHECKS = 1;
