/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : shoa-platform

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 24/01/2019 22:12:32
*/

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `shoa_user`;
CREATE TABLE `shoa_user`  (
  `id` varchar(32) NOT NULL,
  `role_id` varchar(255) DEFAULT NULL COMMENT '角色ID',
  `nickname` varchar(255) DEFAULT NULL COMMENT '用户昵称',
  `username` varchar(255) DEFAULT NULL COMMENT '用户登录名',
  `password` varchar(255) DEFAULT NULL COMMENT '登录密码',
  `create_time` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `user_desc` varchar(255) DEFAULT NULL COMMENT '描述',
  `flag` varchar(3) DEFAULT NULL COMMENT '是否删除标记',
  `delete_time` varchar(32) DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

-- 密码：buxiaofen@123
INSERT INTO `shoa_user` VALUES ('cda473011fe111e989691c872cb135b0', '1550131242206', '卜小凤', 'buxiaofen', 'eb48d62dbe418e42a3e9bd5c2af623b3', '2019-01-24 22:09', '超级管理员', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
