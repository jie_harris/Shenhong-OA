/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : shoa-platform

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 24/01/2019 22:12:18
*/

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `shoa_file`;
CREATE TABLE `shoa_file`  (
  `id` varchar(32) NOT NULL,
  `file_source_name` varchar(255) DEFAULT NULL COMMENT '文件源名称（含后缀）',
  `icon_type` varchar(255) DEFAULT NULL COMMENT '文件图标类型',
  `file_size` varchar(255) DEFAULT NULL COMMENT '文件大小（MB）',
  `file_desc` varchar(255) DEFAULT NULL COMMENT '文件描述',
  `upload_people` varchar(32) DEFAULT NULL COMMENT '文件上传人昵称',
  `upload_time` varchar(32) DEFAULT NULL COMMENT '文件上传时间',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
