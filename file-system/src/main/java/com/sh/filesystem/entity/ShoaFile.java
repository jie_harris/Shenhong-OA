package com.sh.filesystem.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author He changjie
 */
@Table(name = "shoa_file")
public class ShoaFile {

  @Id
  @GeneratedValue(generator = "UUID")
  private String id;
  private String fileSourceName;
  private String iconType;
  private String fileSize;
  private String fileDesc;
  private String uploadPeople;
  private String uploadTime;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFileSourceName() {
    return fileSourceName;
  }

  public void setFileSourceName(String fileSourceName) {
    this.fileSourceName = fileSourceName;
  }

  public String getIconType() {
    return iconType;
  }

  public void setIconType(String iconType) {
    this.iconType = iconType;
  }


  public String getFileSize() {
    return fileSize;
  }

  public void setFileSize(String fileSize) {
    this.fileSize = fileSize;
  }


  public String getFileDesc() {
    return fileDesc;
  }

  public void setFileDesc(String fileDesc) {
    this.fileDesc = fileDesc;
  }


  public String getUploadPeople() {
    return uploadPeople;
  }

  public void setUploadPeople(String uploadPeople) {
    this.uploadPeople = uploadPeople;
  }


  public String getUploadTime() {
    return uploadTime;
  }

  public void setUploadTime(String uploadTime) {
    this.uploadTime = uploadTime;
  }

}
