package com.sh.filesystem.handle;

import com.sh.filesystem.common.Result;
import com.sh.filesystem.common.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常处理类
 * @author Ke.Shi
 * @version 2018-10-09
 */
@ControllerAdvice
public class ExceptionHandle {

    private final static Logger LOGGER = LoggerFactory.getLogger(ExceptionHandle.class);

    /**
     * 通用全局异常处理，包含全部系统异常
     * @return Result
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result exceptionGet(Exception e){
        LOGGER.error("【系统异常】{}", e);
        return ResultUtil.exception(e.getMessage());
    }
}
