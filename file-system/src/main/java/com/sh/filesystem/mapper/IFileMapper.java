package com.sh.filesystem.mapper;

import com.sh.filesystem.base.BaseMapper;
import com.sh.filesystem.entity.ShoaFile;
import org.springframework.stereotype.Component;

/**
 * @Author: he changjie on 2019/2/17
 * @Description:
 */
@Component
public interface IFileMapper extends BaseMapper<ShoaFile> {
}
