/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : shoa-platform

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 24/01/2019 22:12:18
 工作计划
*/

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `shoa_work_plan`;
CREATE TABLE `shoa_work_plan`  (
  `id` varchar(32) NOT NULL,
  `work_name` varchar(255) DEFAULT NULL COMMENT '工作名称',
  `work_time` varchar(32) DEFAULT NULL COMMENT '工作开始时间',
  `process_limite` varchar(32) DEFAULT NULL COMMENT '工作处理时限',
  `address` varchar(255) DEFAULT NULL COMMENT '工作地点',
  `matter` TEXT DEFAULT NULL COMMENT '工作事宜',
  `status` varchar(32) DEFAULT NULL COMMENT '是否已完成（已完成，进行中）',
  `initiator_Id` varchar(32) DEFAULT NULL COMMENT '工作发起人ID',
  `initiator` varchar(32) DEFAULT NULL COMMENT '工作发起人',
  `recipient_Id` varchar(255) DEFAULT NULL COMMENT '工作接收人ID',
  `related_personnel` varchar(255) DEFAULT NULL COMMENT '相关人员',
  `record_time` varchar(32) DEFAULT NULL COMMENT '操作时间',
  `operation_desc` TEXT DEFAULT NULL COMMENT '操作说明',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
