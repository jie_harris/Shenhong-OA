package com.sh.workplan.controller;

import com.google.common.collect.Maps;
import com.sh.workplan.common.JsonMapper;
import com.sh.workplan.common.ResultUtil;
import com.sh.workplan.entity.WorkPlan;
import com.sh.workplan.enums.ResultEnum;
import com.sh.workplan.handle.ExceptionHandle;
import com.sh.workplan.service.WorkPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: he changjie on 2019/3/8
 * @Description:
 */
@RestController
@RequestMapping("/wp")
public class WorkPlanController {
    @Autowired
    private ExceptionHandle exceptionHandle;
    private JsonMapper jsonMapper = JsonMapper.INSTANCE;
    @Autowired
    private WorkPlanService workPlanService;

    /**
     * 发起一个工作计划
     * @param workPlan 工作计划实体
     * @return ResponseEntity
     */
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public ResponseEntity addWorkPlan(@RequestBody WorkPlan workPlan){
        int add = workPlanService.add(workPlan);
        try {
            if(add!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }

    }

    /**
     * 员工提交工作
     * @param workPlan 工作计划实体
     * @return ResponseEntity
     */
    @RequestMapping(value = "/submit",method = RequestMethod.POST)
    public ResponseEntity submitWork(@RequestBody WorkPlan workPlan){
        int submit = workPlanService.submit(workPlan);
        try {
            if(submit!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }

    }

    /**
     * 分页
     * 查询符合搜索条件的所有工作计划
     * @param params 搜索参数
     * @return ResponseEntity
     */
    @RequestMapping(value = "/queryAll/{params}",method = RequestMethod.GET)
    public ResponseEntity queryAll(@PathVariable String params){
        try {
            Map fromJson = jsonMapper.fromJson(params, Map.class);
            String userId = (String)fromJson.get("userId");
            String search = (String)fromJson.get("search");
            int currentPage = (int)fromJson.get("currentPage");
            int pageSize = (int)fromJson.get("pageSize");
            List<WorkPlan> list = workPlanService.queryAll(userId,search,currentPage,pageSize);
            if(list!=null){
                HashMap<String, Object> map = Maps.newHashMap();
                map.put("list",list);
                map.put("total",workPlanService.queryAll(userId,search));
                map.put("currentPage",currentPage);
                map.put("pageSize",pageSize);
                return new ResponseEntity(ResultUtil.success(map), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 通过员工ID查询该期间内有无完成的工作
     * @param params 搜索参数
     * @return ResponseEntity
     */
    @RequestMapping(value = "/queryAllW/{params}",method = RequestMethod.GET)
    public ResponseEntity queryAllS(@PathVariable String params){
        try {
            Map fromJson = jsonMapper.fromJson(params, Map.class);
            String id = (String)fromJson.get("id");
            String beginTime = (String)fromJson.get("beginTime");
            String endTime = (String)fromJson.get("endTime");
            List<WorkPlan> list = workPlanService.queryAll(id,beginTime,endTime);
            if(list!=null){
                return new ResponseEntity(ResultUtil.success(list), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }
}
