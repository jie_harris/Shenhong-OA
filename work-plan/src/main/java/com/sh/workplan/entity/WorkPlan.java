package com.sh.workplan.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author he changjie
 */
@Table(name = "shoa_work_plan")
public class WorkPlan {

  @Id
  @GeneratedValue(generator = "UUID")
  private String id;
  private String workName;
  private String workTime;
  private String processLimite;
  private String address;
  private String matter;
  private String status;
  private String initiatorId;
  private String initiator;
  private String recipientId;
  private String relatedPersonnel;
  private String recordTime;
  private String operationDesc;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getWorkName() {
    return workName;
  }

  public void setWorkName(String workName) {
    this.workName = workName;
  }


  public String getWorkTime() {
    return workTime;
  }

  public void setWorkTime(String workTime) {
    this.workTime = workTime;
  }


  public String getProcessLimite() {
    return processLimite;
  }

  public void setProcessLimite(String processLimite) {
    this.processLimite = processLimite;
  }


  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }


  public String getRelatedPersonnel() {
    return relatedPersonnel;
  }

  public void setRelatedPersonnel(String relatedPersonnel) {
    this.relatedPersonnel = relatedPersonnel;
  }


  public String getMatter() {
    return matter;
  }

  public void setMatter(String matter) {
    this.matter = matter;
  }


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }


  public String getInitiator() {
    return initiator;
  }

  public void setInitiator(String initiator) {
    this.initiator = initiator;
  }

  public String getInitiatorId() {
    return initiatorId;
  }

  public void setInitiatorId(String initiatorId) {
    this.initiatorId = initiatorId;
  }

  public String getRecipientId() {
    return recipientId;
  }

  public void setRecipientId(String recipientId) {
    this.recipientId = recipientId;
  }

  public String getRecordTime() {
    return recordTime;
  }

  public void setRecordTime(String recordTime) {
    this.recordTime = recordTime;
  }


  public String getOperationDesc() {
    return operationDesc;
  }

  public void setOperationDesc(String operationDesc) {
    this.operationDesc = operationDesc;
  }

}
