package com.sh.workplan.service;

import com.sh.workplan.entity.WorkPlan;
import com.sh.workplan.mapper.IWorkPlanMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Author: he changjie on 2019/3/8
 * @Description:
 */
@Service
public class WorkPlanService {
    @Autowired
    private IWorkPlanMapper workMapper;

    /**
     * 发起一个工作计划
     * @param work 工作计划实体
     * @return
     */
    public int add(WorkPlan work){
        work.setStatus("进行中");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = sdf.format(new Date());
        work.setWorkTime(time.substring(0,10));
        return workMapper.insertSelective(work);
    }

    /**
     * 员工提交工作
     * @param work 工作计划实体
     * @return
     */
    public int submit(WorkPlan work){
        WorkPlan workPlan = new WorkPlan();
        workPlan.setId(work.getId());
        workPlan.setOperationDesc(work.getOperationDesc());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = sdf.format(new Date());
        workPlan.setRecordTime(time.substring(0,10));
        workPlan.setStatus("已完成");
        return workMapper.updateByPrimaryKeySelective(workPlan);
    }

    /**
     * 通过员工ID查询该期间内有无完成的工作
     * @param id 员工ID
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @return  List<WorkPlan>
     */
    public List<WorkPlan> queryAll(String id,String beginTime,String endTime){
        Example example=new Example(WorkPlan.class);
        example.setOrderByClause("work_time desc");
        Example.Criteria criteria = example.createCriteria();
        criteria.andLike("initiatorId","%"+id+"%");
        criteria.andEqualTo("status","已完成");
        criteria.andGreaterThanOrEqualTo("recordTime",endTime);
        criteria.andLessThanOrEqualTo("recordTime",beginTime);
        return workMapper.selectByExample(example);
    }

    /**
     * 分页
     * 查询符合搜索条件的所有工作计划
     * @param id 员工编号
     * @param search 工作计划名称
     * @param currentPage 当前页号
     * @param pageSize 页尺寸
     * @return
     */
    public List<WorkPlan> queryAll(String id,String search,int currentPage,int pageSize){
        RowBounds rowBunds = new RowBounds((currentPage - 1) * pageSize, pageSize);
        Example example=new Example(WorkPlan.class);
        example.setOrderByClause("work_time desc");
        Example.Criteria criteria = example.createCriteria();
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("workName","%"+search+"%");
        }
        Example.Criteria criteria1 = example.createCriteria();
        criteria1.orEqualTo("initiatorId",id);
        criteria1.orLike("recipientId","%"+id+"%");
        example.and(criteria1);
        return workMapper.selectByExampleAndRowBounds(example,rowBunds);
    }

    /**
     * 分页
     * 查询符合搜索条件的所有工作计划的数量
     * @param id 员工编号
     * @param search 工作计划名称
     * @return
     */
    public int queryAll(String id,String search){
        Example example=new Example(WorkPlan.class);
        Example.Criteria criteria = example.createCriteria();
        example.setOrderByClause("work_time desc");
        if(StringUtils.isNotEmpty(search)){
            criteria.andLike("workName","%"+search+"%");
        }
        Example.Criteria criteria1 = example.createCriteria();
        criteria1.orEqualTo("initiatorId",id);
        criteria1.orLike("recipientId","%"+id+"%");
        example.and(criteria1);
        return workMapper.selectCountByExample(example);
    }

}
