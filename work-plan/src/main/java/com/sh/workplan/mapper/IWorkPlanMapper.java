package com.sh.workplan.mapper;

import com.sh.workplan.base.BaseMapper;
import com.sh.workplan.entity.WorkPlan;
import org.springframework.stereotype.Component;

/**
 * @Author: he changjie on 2019/3/8
 * @Description:
 */
@Component
public interface IWorkPlanMapper extends BaseMapper<WorkPlan> {
}
