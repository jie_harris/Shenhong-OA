package com.sh.notice.controller;

import com.sh.notice.common.ResultUtil;
import com.sh.notice.entity.Notice;
import com.sh.notice.enums.ResultEnum;
import com.sh.notice.handle.ExceptionHandle;
import com.sh.notice.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @Author: he changjie on 2019/3/7
 * @Description:
 */
@RestController
@RequestMapping("/notice")
public class NoticeController {
    @Autowired
    private ExceptionHandle exceptionHandle;
    @Autowired
    private NoticeService noticeService;

    /**
     * 发布通知
     * @param notice 通知
     * @return ResponseEntity
     */
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public ResponseEntity addNotice(@RequestBody Notice notice){
        int add = noticeService.add(notice);
        try {
            if(add!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }

    }

    /**
     * 将一条通告信息置为已读
     * @param id 通告ID
     * @return ResponseEntity
     */
    @RequestMapping(value = "/read/{id}",method = RequestMethod.GET)
    public ResponseEntity read(@PathVariable("id") String id){
        int add = noticeService.read(id);
        try {
            if(add!=-1){
                return new ResponseEntity(ResultUtil.success(null), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }

    }

    /**
     * 查询所有该用户的未读消息
     * @param receiverId 用户ID
     * @return ResponseEntity
     */
    @RequestMapping(value = "/allUnread/{receiverId}",method = RequestMethod.GET)
    public ResponseEntity queryAllUnreadOfUserId(@PathVariable("receiverId") String receiverId){
        List<Notice> list = noticeService.queryAllUnreadOfUserId(receiverId);
        try {
            if(list!=null){
                return new ResponseEntity(ResultUtil.success(list), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 查询分类查询该用户的未读消息
     * @param receiverId 用户ID
     * @return ResponseEntity
     */
    @RequestMapping(value = "/Unread/{receiverId}",method = RequestMethod.GET)
    public ResponseEntity queryUnreadOfUserId(@PathVariable("receiverId") String receiverId){
        Map<String, Object> map = noticeService.queryUnreadOfUserId(receiverId);
        try {
            if(map!=null){
                return new ResponseEntity(ResultUtil.success(map), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

    /**
     * 查询折线图和环形图的数据
     * @param receiverId 用户ID
     * @return Map<String ,Object>
     */
    @RequestMapping(value = "/count/{receiverId}",method = RequestMethod.GET)
    public ResponseEntity queryAllCountOfUserId(@PathVariable("receiverId") String receiverId){
        Map<String, Object> map = noticeService.queryAllCountOfUserId(receiverId);
        try {
            if(map!=null){
                return new ResponseEntity(ResultUtil.success(map), HttpStatus.OK);
            }else{
                return new ResponseEntity(ResultUtil.error(ResultEnum._FAILED), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(exceptionHandle.exceptionGet(e), HttpStatus.OK);
        }
    }

}
