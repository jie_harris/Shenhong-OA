package com.sh.notice.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sh.notice.entity.Notice;
import com.sh.notice.mapper.INoticeMapper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Author: he changjie on 2019/3/7
 * @Description:
 */
@Service
public class NoticeService {
    @Autowired
    private INoticeMapper noticeMapper;

    /**
     * 发布通知
     * @param notice 通知
     * @return 添加的数量
     */
    public int add(Notice notice){
        int count=0;
        if (StringUtils.isNotEmpty(notice.getReceiverId())) {
            String[] split = notice.getReceiverId().split("[,，]");
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String format = sdf.format(new Date());
            notice.setCreateTime(format);
            notice.setStatus("0");
            for (int i=0;i<split.length;i++) {
                notice.setReceiverId(split[i]);
                count=noticeMapper.insertSelective(notice);
            }
        }
        return count;
    }

    /**
     * 将一条通告信息置为已读
     * @param id 通告ID
     * @return 修改的数量
     */
    public int read(String id){
        Notice notice = noticeMapper.selectByPrimaryKey(id);
        notice.setStatus("1");
        return noticeMapper.updateByPrimaryKeySelective(notice);
    }

    /**
     * 查询所有该用户的未读消息
     * @param receiverId 用户ID
     * @return List<Notice>
     */
    public List<Notice> queryAllUnreadOfUserId(String receiverId){
        Example example = new Example(Notice.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("receiverId",receiverId);
        criteria.andEqualTo("status","0");
        return noticeMapper.selectByExample(example);
    }

    /**
     * 查询分类查询该用户的未读消息
     * @param receiverId 用户ID
     * @return Map<String ,Object>
     */
    public Map<String ,Object> queryUnreadOfUserId(String receiverId){
        Example example = new Example(Notice.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("receiverId",receiverId);
        criteria.andEqualTo("status","0");
        List<Notice> list = noticeMapper.selectByExample(example);
        List<Notice> daily=new ArrayList<>();
        List<Notice> finance=new ArrayList<>();
        List<Notice> hr=new ArrayList<>();
        List<Notice> work=new ArrayList<>();
        HashMap<String, Object> map = Maps.newHashMap();
        for (Notice notice : list) {
            switch (notice.getCategory()){
                case "日常通告":daily.add(notice);break;
                case "财务通告":finance.add(notice);break;
                case "人事通告":hr.add(notice);break;
                case "工作通告":work.add(notice);break;
                default:break;
            }
        }
        map.put("daily",daily);
        map.put("finance",finance);
        map.put("hr",hr);
        map.put("work",work);
        return map;
    }

    /**
     * 查询折线图和环形图的数据
     * @param receiverId 用户ID
     * @return Map<String ,Object>
     */
    public Map<String ,Object> queryAllCountOfUserId(String receiverId){
        //环形图数据
        Example example = new Example(Notice.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("receiverId",receiverId);
        criteria.orEqualTo("publisherId",receiverId);
        List<Notice> list = noticeMapper.selectByExample(example);
        List<Integer> daily=new ArrayList<>(),
                      finance=new ArrayList<>(),
                      hr=new ArrayList<>(),
                      work=new ArrayList<>();
        HashMap<String, Object> map = Maps.newHashMap();
        int read1=0,unread1=0,send1=0;
        int read2=0,unread2=0,send2=0;
        int read3=0,unread3=0,send3=0;
        int read4=0,unread4=0,send4=0;
        for (Notice notice : list) {
            switch (notice.getCategory()){
                case "日常通告":{
                    if(receiverId.equals(notice.getPublisherId())){
                        send1++;
                    }else{
                        if("0".equals(notice.getStatus())){
                            unread1++;
                        }else{
                            read1++;
                        }
                    }
                }break;
                case "财务通告":{
                    if(receiverId.equals(notice.getPublisherId())){
                        send2++;
                    }else{
                        if("0".equals(notice.getStatus())){
                            unread2++;
                        }else{
                            read2++;
                        }
                    }
                }break;
                case "人事通告":{
                    if(receiverId.equals(notice.getPublisherId())){
                        send3++;
                    }else{
                        if("0".equals(notice.getStatus())){
                            unread3++;
                        }else{
                            read3++;
                        }
                    }
                }break;
                case "工作通告":{
                    if(receiverId.equals(notice.getPublisherId())){
                        send4++;
                    }else{
                        if("0".equals(notice.getStatus())){
                            unread4++;
                        }else{
                            read4++;
                        }
                    }
                }break;
                default:break;
            }
        }
        daily.add(read1+unread1+send1);
        daily.add(read1);
        daily.add(unread1);
        daily.add(send1);
        finance.add(read2+unread2+send2);
        finance.add(read2);
        finance.add(unread2);
        finance.add(send2);
        hr.add(read3+unread3+send3);
        hr.add(read3);
        hr.add(unread3);
        hr.add(send3);
        work.add(read4+unread4+send4);
        work.add(read4);
        work.add(unread4);
        work.add(send4);
        HashMap<Object, Object> map1 = Maps.newHashMap();
        map1.put("daily",daily);
        map1.put("finance",finance);
        map1.put("hr",hr);
        map1.put("work",work);

        map.put("hxt",map1);

        //折线图数据
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        Date now = new Date();
        String beginTime,endTime;
        ArrayList<String> dateList1 = Lists.newArrayList();
        ArrayList<Integer> dateList2 = Lists.newArrayList();
        ArrayList<Integer> dateList3 = Lists.newArrayList();
        ArrayList<Integer> dateList4 = Lists.newArrayList();
        ArrayList<Integer> dateList5 = Lists.newArrayList();
        for(int i=-7;i<0;i++){
            calendar.setTime(now);
            calendar.add(Calendar.DATE,i);
            beginTime = sdf.format(calendar.getTime());
            calendar.setTime(now);
            calendar.add(Calendar.DATE,i+1);
            endTime = sdf.format(calendar.getTime());
            List<Notice> notices = noticeMapper.queryDate(beginTime, endTime);
            int rc = 0,rs = 0,cw = 0,gz = 0;
            if(notices != null && notices.size()>0){
                for (Notice notice : notices) {
                    switch (notice.getCategory()) {
                        case "日常通告":rc+=Integer.valueOf(notice.getStatus());break;
                        case "人事通告":rs+=Integer.valueOf(notice.getStatus());break;
                        case "财务通告":cw+=Integer.valueOf(notice.getStatus());break;
                        case "工作通告":gz+=Integer.valueOf(notice.getStatus());break;
                        default:break;
                    }
                }
            }
            dateList1.add(beginTime.substring(5,10));
            dateList2.add(rc);
            dateList3.add(rs);
            dateList4.add(cw);
            dateList5.add(gz);
        }
        HashMap<Object, Object> map2 = Maps.newHashMap();
        map2.put("x",dateList1);
        map2.put("rc",dateList2);
        map2.put("rs",dateList3);
        map2.put("cw",dateList4);
        map2.put("gz",dateList5);

        map.put("zxt",map2);
        return map;
    }
}
