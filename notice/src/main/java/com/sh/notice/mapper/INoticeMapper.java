package com.sh.notice.mapper;

import com.sh.notice.base.BaseMapper;
import com.sh.notice.entity.Notice;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: he changjie on 2019/2/10
 * @Description:
 */
@Component
public interface INoticeMapper extends BaseMapper<Notice> {
    /**
     * 获取期间的通告数量
     * @param begin 开始时间
     * @param end 结束时间
     * @return List<Notice>
     */
    List<Notice> queryDate(@Param("begin") String begin, @Param("end") String end);
}
