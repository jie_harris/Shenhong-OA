/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : shoa-platform

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 24/01/2019 22:12:18
 通告信息
*/

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `shoa_notice`;
CREATE TABLE `shoa_notice`  (
  `id` varchar(32) NOT NULL,
  `notice_title` varchar(255) DEFAULT NULL COMMENT '通知标题',
  `notice_content` TEXT DEFAULT NULL COMMENT '通知内容',
  `category` varchar(255) DEFAULT NULL COMMENT '通知类型（日常、工作、财务、人事）',
  `publisher_id` varchar(255) DEFAULT NULL COMMENT '通知发布人ID',
  `receiver_id` varchar(255) DEFAULT NULL COMMENT '通知接收人ID',
  `create_time` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `status` varchar(32) DEFAULT NULL COMMENT '通知状态(1:已读、0:未读)',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
